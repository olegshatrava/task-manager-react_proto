FROM node:8.11-alpine

RUN mkdir -p /src/app

RUN npm i npm@latest -g

WORKDIR /src/app
COPY ["package.json", ".npmrc*", "yarn.lock*", "./"]

RUN npm install -g yarn
RUN yarn install

COPY . /src/app

EXPOSE 9000

CMD yarn run dev
