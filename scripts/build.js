/* eslint no-console: 0 */

import webpack from 'webpack'
import webpackConfig from '../tools/webpack/webpack.base.babel.js'

// eslint-disable-next-line no-unused-vars
const [x, y, ...args] = process.argv

// Check optimize mode
const isOptimize = args.some(arg => arg === '--optimize')

const compiler = webpack(webpackConfig({ target: 'client', isOptimize }))

compiler.run((err, stats) => {
	if (err) {
		return console.error(err)
	}

	console.log(stats.toString({ colors: true }))
})
