/* eslint no-console: 0 */
/* eslint-disable-next-line no-unused-vars */
import chalk from 'chalk'

/**
*	Logger messages in console.
*
*	@param { string } title - Title log
*	@param { string } message - Message log
*	@param { string } level - Type log (e.g 'warn' || 'error' || 'info').
*
*	@return { string } - Output message in console.
*/
export function log({ title, message, level = 'info' }) {
	const endTitle = title ? `${ title.toUpperCase() }: ` : ''

	const msg = `${ endTitle }${ message }`

	switch (level) {
		case 'warn':
			console.log(chalk.yellow(msg))
			break

		case 'error':
			console.log(chalk.bgRed.yellow(msg))
			break

		case 'info':
		default:
			console.log(chalk.green(msg))
	}
}
