/**
* Filters out all null/undefined/false items from the given array.
*
* @param  {Array} as - the target array
*
* @return {Array} The filtered array.
*/

export function removeNull(as) {
	return as.filter(Boolean)
}
