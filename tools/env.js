import fs from 'fs'

import paths from './paths'
import { removeNull } from './utils/removeNull'

const NODE_ENV = process.env.NODE_ENV

if (!NODE_ENV) {
	throw new Error('The NODE_ENV environment variable is required but was not specified.')
}

var dotenvFiles = removeNull([
	`${ paths.dotenv }.${ NODE_ENV }.local`,
	`${ paths.dotenv }.${ NODE_ENV }`,
	NODE_ENV !== 'test' && `${ paths.dotenv }.local`,
	paths.dotenv
])

dotenvFiles.forEach(dotenvFile => {
	if (fs.existsSync(dotenvFile)) {
		require('dotenv-expand')(
			require('dotenv').config({
				path: dotenvFile
			})
		)
	}
})

// Grab NODE_ENV and REACT__* environment variables and prepare them to be
// injected into the application via DefinePlugin in Webpack configuration.
const REGX_ENV = /^REACT_/i
const FLAG_ENV = /^FLAG_/i

function getClientEnvironment() {
	const raw = Object.keys(process.env)
	.filter(key => REGX_ENV.test(key) || FLAG_ENV.test(key))
	.reduce(
		(env, key) => {
			env[key] = process.env[key]
			return env
		},
		{
			NODE_ENV: process.env.NODE_ENV || 'development',
			HOST: process.env.HOST || 'localhost',
			PORT: process.env.PORT || '5000'
		}
	)

	// Stringify all values so we can feed into Webpack DefinePlugin
	const stringified = {
		'process.env': Object.keys(raw).reduce((env, key) => {
			env[key] = JSON.stringify(raw[key])
			return env
		}, {})
	}

	return { raw, stringified }
}

module.exports = getClientEnvironment
