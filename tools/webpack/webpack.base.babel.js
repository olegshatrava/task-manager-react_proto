import path from 'path'
import chalk from 'chalk'
import webpack from 'webpack'
import ProgressBarPlugin from 'progress-bar-webpack-plugin'
import CleanWebpackPlugin from 'clean-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import OptimizeCSSAssetsPlugin from 'optimize-css-assets-webpack-plugin'
import SWPrecacheWebpackPlugin from 'sw-precache-webpack-plugin'
import UglifyJsPlugin from 'uglifyjs-webpack-plugin'
import SvgSpritePlugin from 'svg-sprite-loader/plugin'
import FriendlyErrors from 'friendly-errors-webpack-plugin'
import CompressionPlugin from 'compression-webpack-plugin'
// import BrotliPlugin from 'brotli-webpack-plugin'
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer'

import paths from '../paths'
import getClientEnvironment from '../env'
import { ifElse, removeNull, log } from '../utils'

const env = getClientEnvironment()

/**
*	Generates a webpack configuration for the target configuration.
*
*	@param { string } target - The bundle target (e.g 'clinet' || 'server').
*	@param { boolean } isOptimize - Build an optimised version of the bundle?
*
*	@return { Object } - The webpack configuration.
*/
export default function webpackConfig({ target, isOptimize = false }) {
	if (!target) {
		return new Error('No bundle configuration exists for target: ', target)
	}

	const NODE_ENV = env.raw['NODE_ENV']

	const isDev = NODE_ENV === 'development'
	const isProd = NODE_ENV === 'production'
	const isClient = target === 'client'
	// const isServer = target === 'server'

	const ifDev = ifElse(isDev)
	const ifProd = ifElse(isProd)
	const ifDevClient = ifElse(isDev && isClient)
	const ifProdMinify = ifElse(isProd && isOptimize)

	log({
		title: 'Webpack',
		message: `Creating a "${ isProd ? 'production' : 'development' }" bundle configuration for the "${ target }"`,
		level: 'info'
	})

	return {
		name: 'client',
		target: 'web',
		context: paths.root,

		mode: isProd ? 'production' : 'development',

		devtool: ifProdMinify(false, 'cheap-module-source-map'),

		node: {
			__dirname: true,
			__filename: true
		},

		entry: {
			bundle: removeNull([
				'babel-polyfill',
				ifDevClient(`webpack-hot-middleware/client?reload=true`),
				'./src/index.js'
			]),
			vendor: ['react', 'react-dom']
		},

		output: {
			filename: `assets/scripts/${ ifProdMinify('[name].[hash:16]', '[name]') }.js`,
			chunkFilename: `assets/scripts/${ ifProdMinify('[name].[hash:16]', '[name]') }.chunk.js`,
			path: paths.buildUrl,
			publicPath: '/'
		},

		resolve: {
			extensions: ['.js', '.jsx', '.scss', '.css', '.json'],
			modules: [
				'node_modules',
				path.resolve(paths.srcUrl)
			]
		},

		module: {
			strictExportPresence: true,
			rules: [
				{
					test: /\.(js|jsx)$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader',
						options: {
							cacheDirectory: true
						}
					}
				}, {
					test: /\.(css|scss)$/,
					use: [
						{
							loader: ifDev('style-loader', MiniCssExtractPlugin.loader)
						}, {
							loader: 'css-loader',
							options: {
								modules: true,
								camelCase: true,
								localIdentName: ifProdMinify('[hash:base64:7]', '[name]-[local]--[hash:base64:7]'),
								importLoaders: 1,
								sourceMap: true
							}
						}, {
							loader: 'postcss-loader'
						}, {
							loader: 'sass-loader'
						},
						{
							loader: 'sass-resources-loader',
							options: {
								resources: './src/styles/shared/mixins/**/*.scss'
							}
						}
					]
				}, {
					test: /\.(jpe?g|gif|png|svg)$/,
					use: {
						loader: 'url-loader',
						options: {
							limit: 10000,
							name: `images/${ ifProdMinify('[name].[hash:8]', '[path][name]') }.[ext]`
						}
					}
				}, {
					test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
					use: [{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							outputPath: 'fonts/'
						}
					}]
				}
			]
		},

		plugins: removeNull([
			new CleanWebpackPlugin(['build'], { root: paths.root }),

			new ProgressBarPlugin({
				format: `WEBPACK: build [:bar] ${ chalk.green(':percent') } (:elapsed seconds)`,
				width: 30,
				clear: false
			}),

			new HtmlWebpackPlugin({
				template: path.resolve(paths.publicUrl, 'index.html'),
				minify: ifProdMinify(() => ({
					removeComments: true,
					collapseWhitespace: true,
					removeRedundantAttributes: true,
					useShortDoctype: true,
					removeEmptyAttributes: true,
					removeStyleLinkTypeAttributes: true,
					keepClosingSlash: true,
					minifyJS: true,
					minifyCSS: true,
					minifyURLs: true
				}), {})
			}),

			// Minimize and generation file css styles
			new MiniCssExtractPlugin({
				filename: `assets/styles/${ ifProdMinify('main.[contenthash:16]', 'main') }.css`,
				chunkFilename: `assets/styles/${ ifProdMinify('[name].[contenthash:16]', '[name]') }.css`
			}),

			// Extract svg generation
			ifProd(() => new SvgSpritePlugin({ plainSprite: true })),

			// Compress static to gzip
			ifProd(() => new CompressionPlugin({
				test: /\.js$|\.html$|\.css$/,
				algorithm: 'gzip',
				minRatio: 0.8
			})),

			/*
			*	Compress static to brotli
			*	Enable plugin with nodejs server in production
			*/
			// ifProd(() => new BrotliPlugin()),

			ifDev(() => new FriendlyErrors()),
			ifDevClient(() => new webpack.NamedModulesPlugin()),
			ifDevClient(() => new webpack.HotModuleReplacementPlugin()),

			new webpack.DefinePlugin({
				'__CLIENT__': JSON.stringify(isClient),
				'__SERVER__': JSON.stringify(false),
				'__REDUX_DEVTOOLS__': JSON.stringify(isDev),
				'typeof window': JSON.stringify(isClient ? 'object' : 'undefined'),
				...env.stringified
			}),

			// ServiceWorker precache settings
			ifProd(() => new SWPrecacheWebpackPlugin({
				cacheId: 'openhub',
				dontCacheBustUrlsMatching: /\.\w{8}\./,
				filename: 'sw.js',
				minify: true,
				navigateFallback: '/index.html',
				staticFileGlobsIgnorePatterns: [/\.map$/, /asset-manifest\.json$/]
			})),

			new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),

			// Reports
			ifProd(() => new BundleAnalyzerPlugin({
				analyzerMode: 'static',
				reportFilename: './reports/bundle-analyzer.html',
				openAnalyzer: false
			}))
		]),

		performance: {
			hints: false
		},

		optimization: {
			minimizer: removeNull([
				ifProdMinify(() => new OptimizeCSSAssetsPlugin({
					assetNameRegExp: /.css$/g,
					cssProcessor: require('cssnano'),
					cssProcessorOptions: {
						discardDuplicates: { removeAll: true },
						discardComments: { removeAll: true }
					},
					canPrint: true
				})),

				ifProdMinify(() => new UglifyJsPlugin({
					cache: true,
					parallel: true,
					uglifyOptions: {
						compress: {
							warnings: false,
							comparisons: false
						},
						output: {
							comments: false,
							ascii_only: true
						}
					}
				}))
			]),
			runtimeChunk: {
				name: 'manifest'
			},
			splitChunks: {
				chunks: 'all',
				minChunks: 1,
				automaticNameDelimiter: '-',
				maxAsyncRequests: 6,
				maxInitialRequests: 6,
				cacheGroups: {
					vendor: {
						name: 'vendor',
						chunks: 'initial',
						test: /[\\/]node_modules[\\/]/,
						priority: -10
					},
					common: {
						name: 'common',
						chunks: 'all',
						minChunks: 7,
						minSize: 50000,
						priority: -20,
						enforce: true,
						reuseExistingChunk: true
					}
				}
			}
		}
	}
}
