// @flow
const fs = require('fs')
const path = require('path')

const rootDirectory: string = fs.realpathSync(process.cwd())
const resolvePath = (relativePath: string): string => path.resolve(rootDirectory, relativePath)

module.exports = {
	root: resolvePath(''),
	dotenv: resolvePath('.env'),
	srcUrl: resolvePath('src'),
	publicUrl: resolvePath('public'),
	buildUrl: resolvePath('build')
}
