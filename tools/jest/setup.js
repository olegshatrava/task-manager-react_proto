import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

if (process.env.NODE_ENV === 'test') {
	require('raf').polyfill(global)
}
