import React, { Component } from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
// import { axios } from 'helpers/api'

import { Container, Header, Sidebar } from './shared'

import ProfilePage from './Profile'
import MyTasks from './MyTasks'
import ProjectsPage from './Projects'
import CardInfo from './CardInfo'

class LkModule extends Component {
	prevLocation = this.props.location

	// async componentDidMount() {
	// 	try {
	// 		const { data } = await axios.get('/projects')
	// 		console.log(data)
	// 	} catch (err) {
	// 		console.log(err.response)
	// 	}
	// }

	componentDidUpdate(nextProps) {
		const { location } = this.props

		if (nextProps.history.action !== 'POP' && (!location.state || !location.state.modal)) {
			this.prevLocation = this.props.location
		}
	}

	render() {
		const { location } = this.props
		const isModal = !!(location.state && location.state.modal && this.prevLocation !== location)

		return (
			<Container>
				<Sidebar />

				<Container.Content>
					<Header />

					<Switch location={ isModal ? this.prevLocation : location }>
						<Route path="/tasks" component={ MyTasks } />
						<Route path="/projects/d/:departmentId" component={ ProjectsPage } />
						<Route path="/profile" component={ ProfilePage } />
						<Route path="/c/new" component={ CardInfo } />
						<Route path="/c/:cardId" component={ CardInfo } />
						<Redirect from="/" to="/tasks" />
					</Switch>

					<Switch>
						{ isModal && <Route path="/c/new" component={ CardInfo } /> }
						{ isModal && <Route path="/c/:cardId" component={ CardInfo } /> }
					</Switch>
				</Container.Content>
			</Container>
		)
	}
}

export default LkModule
