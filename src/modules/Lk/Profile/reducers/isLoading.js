import * as types from '../constants'

export const isLoading = (state = false, action) => {
    switch (action.type) {
        case types.FETCH_USERINFO_LOADING: {
			return true
		}

		case types.FETCH_USERINFO_SUCCESS:
		case types.FETCH_USERINFO_FAILED:
		case 'logout/SUCCESS': {
			return false
		}

        default:
            return state
    }
}
