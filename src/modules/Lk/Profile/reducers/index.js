import { combineReducers } from 'redux'

import { isLoading } from './isLoading'
import { isLoad } from './isLoad'
import { basicInfo } from './basicInfo'
import { photoUser } from './photo'
import { emailUserInfo } from './email'
import { phoneUserInfo } from './phone'

export const userInfoCombine = combineReducers({
	isLoading,
	isLoad,
	basicInfo: basicInfo,
	photo: photoUser,
	email: emailUserInfo,
	phone: phoneUserInfo
})
