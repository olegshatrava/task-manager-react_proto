import * as types from '../constants'

const initialState = {
	path: '//placehold.it/500x500',
	thumb: '//placehold.it/150x150'
}

export const photoUser = (state = initialState, action) => {
    switch (action.type) {
		case types.FETCH_USERINFO_LOADING:
		case types.FETCH_USERINFO_FAILED: {
			return initialState
		}

		case types.FETCH_USERINFO_SUCCESS: {
			const { payload } = action

			if (payload.image !== null) {
				return {
					...state,
					path: payload.image.path,
					thumb: payload.image.thumb
				}
			}

			return initialState
		}

		case types.CHANGE_USER_PHOTO_SUCCESS: {
			const { payload } = action

			return {
				...state,
				path: payload.path,
				thumb: payload.thumb
			}
		}

        default:
            return state
    }
}
