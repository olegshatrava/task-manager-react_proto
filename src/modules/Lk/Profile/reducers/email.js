import * as types from '../constants'

const initialState = null

export const emailUserInfo = (state = initialState, action) => {
    switch (action.type) {
		case types.FETCH_USERINFO_LOADING:
		case types.FETCH_USERINFO_FAILED: {
			return null
		}

		case types.FETCH_USERINFO_SUCCESS: {
			const { payload } = action

			return {
				...state,
				active: true,
				value: payload.email
			}
		}

		case types.CHANGE_USEREMAIL_SUCCESS: {
			const { payload } = action

			return {
				...state,
				value: payload.email
			}
		}

        default:
            return state
    }
}
