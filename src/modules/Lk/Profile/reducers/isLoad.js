import * as types from '../constants'

export const isLoad = (state = false, action) => {
    switch (action.type) {
		case types.FETCH_USERINFO_LOADING:
		case types.FETCH_USERINFO_FAILED:
		case 'logout/SUCCESS': {
			return false
		}

		case types.FETCH_USERINFO_SUCCESS: {
			return true
		}

        default:
            return state
    }
}
