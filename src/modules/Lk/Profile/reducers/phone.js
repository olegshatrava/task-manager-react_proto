import * as types from '../constants'

const initialState = null

export const phoneUserInfo = (state = initialState, action) => {
    switch (action.type) {
		case types.FETCH_USERINFO_LOADING:
		case types.FETCH_USERINFO_FAILED: {
			return null
		}

		case types.FETCH_USERINFO_SUCCESS: {
			const { payload } = action

			return {
				...state,
				active: true,
				value: payload.phone
			}
		}

		case types.CHANGE_USERPHONE_SUCCESS: {
			const { payload } = action

			return {
				...state,
				value: payload.phone
			}
		}

        default:
            return state
    }
}
