import * as types from '../constants'

const initialState = null

export const basicInfo = (state = initialState, action) => {
    switch (action.type) {
		case types.FETCH_USERINFO_LOADING:
		case types.FETCH_USERINFO_FAILED: {
			return null
		}

		case types.FETCH_USERINFO_SUCCESS:
		case types.EDIT_BASICINFO_SUCCESS: {
			const { payload } = action

			return {
				...state,
				id: payload.id,
				firstName: payload.first_name,
				lastName: payload.last_name,
				middleName: payload.middle_name,
				userName: payload.username,
				birthday: payload.birthday,
				city: payload.city || null
			}
		}

        default:
            return state
    }
}
