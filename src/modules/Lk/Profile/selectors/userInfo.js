export const metaUserInfoRequest = (state) => ({
	isLoading: state.userInfo.isLoading,
	isLoad: state.userInfo.isLoad
})

export const getUserBasicInfo = (state) => state.userInfo.basicInfo
export const getUserPhoto = (state) => state.userInfo.photo
export const getUserPhone = (state) => state.userInfo.phone
export const getUserEmail = (state) => state.userInfo.email
