import { object, string } from 'yup'

export function getFormSchema(value) {
	return object().shape({
		lastName: string()
			.max(255)
			.required(),

		firstName: string()
			.max(255)
			.required(),

		middleName: string()
			.max(255),

		username: string()
			.max(255)
	})
}
