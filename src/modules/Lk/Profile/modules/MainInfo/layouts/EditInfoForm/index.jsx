import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Form as FinalForm, Field } from 'react-final-form'
import { Form } from 'antd'
import { Input, Button, Alert, Row, Col } from 'ui/components'

import { validation } from 'helpers/formValidation'
import { getFormSchema } from './formSchema'

import { getUserBasicInfo } from '../../../../selectors/userInfo'
import { editUserInfo } from '../../../../actions/editUserInfo'

const EditInfoForm = ({ userBasicInfo, editInfoSubmit }) => {
	const initialValues = {
		lastName: userBasicInfo.lastName,
		firstName: userBasicInfo.firstName,
		middleName: userBasicInfo.middleName,
		username: userBasicInfo.userName,
		birthday: userBasicInfo.birthday,
		city: userBasicInfo.city
	}

	return (
		<FinalForm
			onSubmit={ editInfoSubmit }
			validate={ validation(getFormSchema) }
			initialValues={ initialValues }
			render={ ({ pristine, submitting, submitSucceeded, dirtySinceLastSubmit, handleSubmit, ...rest }) => (
				<Row>
					<Col md={ 8 }>
						<Form onSubmit={ handleSubmit } noValidate>
							<Row>
								<Col md={ 6 }>
									<Form.Item>
										<Field
											type="text"
											name="lastName"
											label="Фамилия"
											placeholder="Фамилия"
											component={ Input }
										/>
									</Form.Item>
								</Col>

								<Col md={ 6 }>
									<Form.Item>
										<Field
											type="text"
											name="firstName"
											placeholder="Имя"
											component={ Input }
										/>
									</Form.Item>
								</Col>

								<Col md={ 6 }>
									<Form.Item>
										<Field
											type="text"
											name="middleName"
											placeholder="Отчество"
											component={ Input }
										/>
									</Form.Item>
								</Col>

								<Col md={ 6 }>
									<Form.Item>
										<Field
											type="text"
											name="birthday"
											placeholder="Дата рождения"
											component={ Input }
										/>
									</Form.Item>
								</Col>

								<Col md={ 6 }>
									<Form.Item>
										<Field
											type="text"
											name="username"
											placeholder="Имя пользователя"
											component={ Input }
										/>
									</Form.Item>
								</Col>

								{/* <Col md={ 6 }>
									<Form.Item>
										<Field
											type="text"
											name="city"
											placeholder="Город"
											component={ Input }
										/>
									</Form.Item>
								</Col> */}
							</Row>

							{ (!!submitSucceeded && !dirtySinceLastSubmit) && <Form.Item><Alert type="success" title="Данные успешно сохранены" showIcon /></Form.Item> }

							<Form.Item>
								<Button type="submit" theme="success" disabled={ pristine || submitting }>Сохранить</Button>
							</Form.Item>
						</Form>
					</Col>
				</Row>
			) }
		/>
	)
}

const mapStateToProps = (state) => ({
	userBasicInfo: getUserBasicInfo(state)
})

const mapDispatchToProps = (dispatch) => ({
	editInfoSubmit: bindActionCreators(editUserInfo, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(EditInfoForm)
