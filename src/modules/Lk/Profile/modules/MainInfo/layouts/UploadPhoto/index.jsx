import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'

import { Button, Divider, Modal, Dropzone } from 'ui/components'

import { changeUserPhoto } from 'modules/Lk/Profile/actions/changeUserPhoto'

class UploadUserPhoto extends Component {
	state = {
		file: null,
		isOpenModal: false
	}

	handleOpenModal = () => this.setState({ isOpenModal: true })
	handleCloseModal = () => this.setState({ isOpenModal: false })

	handleDropPhoto = (acceptedFiles, rejectedFiles) => {
		if (acceptedFiles.length === 0) return

		const fd = new FormData()
		fd.append('file', acceptedFiles[0])

		this.props.dispatch(changeUserPhoto(fd, this.handleCloseModal))

		// const reader = new FileReader()
		// reader.onload = () => {
		// 	const file = reader.result
		// 	console.log(file)
		// }

		// reader.onabort = () => console.log('file reading was aborted')
		// reader.onerror = () => console.log('file reading has failed')
		// reader.onprogress = (params) => console.log('file reading has failed', params)

		// reader.readAsArrayBuffer(file)
	}

	render() {
		return (
			<Fragment>
				{ this.props.children({ onOpenModal: this.handleOpenModal }) }

				<Modal
					title="Добавление фотографии"
					onClose={ this.handleCloseModal }
					visible={ this.state.isOpenModal }
				>
					<form>
						<Dropzone
							onDrop={ this.handleDropPhoto }
							multiple={ false }
							inputProps={ {
								name: 'photo',
								multiple: false
							} }
						>
							Выберите или перетащите фотографию в область загрузки.
						</Dropzone>

						<Divider />

						<Button type="button" theme="purple" onClick={ this.handleCloseModal } block>Обновить фотографию</Button>
					</form>
				</Modal>
			</Fragment>
		)
	}
}

export default connect()(UploadUserPhoto)
