import React from 'react'
import { connect } from 'react-redux'
import { Button, Avatar, Heading, Paragraph } from 'ui/components'

import { getUserPhoto } from 'modules/Lk/Profile/selectors/userInfo'
import UploadUserPhoto from '../UploadPhoto'

import styles from './PreviewInfo.scss'

const PreviewInfo = ({ userInfo, photoUser }) => (
	<div className={ styles['container'] }>
		<div className={ styles['photo'] }>
			<Avatar
				src={ photoUser.path }
				size="lg"
				shape="circle"
			/>

			<UploadUserPhoto>
				{ ({ onOpenModal }) => (
					<span className={ styles['photo-action'] }>
						<Button theme="purple" shape="circle" icon="camera" onClick={ onOpenModal } />
					</span>
				) }
			</UploadUserPhoto>
		</div>

		<div className={ styles['content'] }>
			<Heading type="h4" className={ styles['userName'] }>
				{ [userInfo.firstName, userInfo.lastName].join(' ') }
			</Heading>

			{ !!userInfo.userName && <Paragraph>{ `@${ userInfo.userName }` }</Paragraph> }
		</div>
	</div>
)

const mapStateToProps = (state) => ({
	photoUser: getUserPhoto(state)
})

export default connect(mapStateToProps)(PreviewInfo)
