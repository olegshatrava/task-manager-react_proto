import React from 'react'
import { connect } from 'react-redux'

import PreviewInfo from './layouts/PreviewInfo'
import EditInfoForm from './layouts/EditInfoForm'
import { getUserBasicInfo } from '../../selectors/userInfo'

export const MainInfoUserProfile = ({ userBasicInfo }) => {
	if (!userBasicInfo) {
		return 'Загрузка...'
	}

	return (
		<div style={ { padding: '15px' } }>
			<PreviewInfo userInfo={ userBasicInfo } />

			<EditInfoForm userInfo={ userBasicInfo } />
		</div>
	)
}

const mapStateToProps = (state) => ({
	userBasicInfo: getUserBasicInfo(state)
})

export default connect(mapStateToProps)(MainInfoUserProfile)
