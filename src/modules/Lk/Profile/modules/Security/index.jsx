import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import Divider from 'ui/components/Divider'
import { metaUserInfoRequest } from '../../selectors/userInfo'

import PasswordChange from './layouts/Password'
import PhoneChange from './layouts/Phone'
import EmailChange from './layouts/Email'

const ProfileSecurity = ({ meta, phone }) => {
	if (!meta.isLoad) {
		return 'Загрузка...'
	}

	return (
		<Fragment>
			<PasswordChange />

			<Divider />

			<PhoneChange />

			<Divider />

			<EmailChange />
		</Fragment>
	)
}

const mapStateToProps = (state) => ({
	meta: metaUserInfoRequest(state)
})

export default connect(mapStateToProps)(ProfileSecurity)
