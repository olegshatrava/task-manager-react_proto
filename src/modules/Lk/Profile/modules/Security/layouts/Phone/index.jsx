import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import { Form as FinalForm, Field } from 'react-final-form'
import { Form } from 'antd'
import { Input, Button, Alert, Row, Col, Heading, Paragraph } from 'ui/components'

import { getUserPhone } from '../../../../selectors/userInfo'
import { changeUserPhone } from '../../../../actions/changeUserPhone'

import { validation } from 'helpers/formValidation'
import { getFormSchema } from './formSchema'

const ChangePhoneUser = ({ currentPhone, submitForm }) => {
	const initialValues = {
		phone: currentPhone.value,
		old_password: ''
	}

	return (
		<Fragment>
			<Heading type="h3">Изменить номер телефона</Heading>

			<Paragraph>Указанный мобильный телефон используется для входа в систему.</Paragraph>

			<FinalForm
				onSubmit={ submitForm }
				validate={ validation(getFormSchema) }
				initialValues={ initialValues }
				render={ ({ pristine, submitting, submitSucceeded, dirtySinceLastSubmit, handleSubmit }) => (
					<Row>
						<Col md={ 4 }>
							<Form onSubmit={ handleSubmit } noValidate>
								<Form.Item>
									<Field
										type="tel"
										name="phone"
										placeholder="Номер телефона"
										component={ Input }
									/>
								</Form.Item>

								{ !pristine && (
									<Form.Item>
										<Field
											type="password"
											name="oldPassword"
											placeholder="Текущий пароль"
											component={ Input }
										/>
									</Form.Item>
								) }

								{ (!!submitSucceeded && !dirtySinceLastSubmit) && <Form.Item><Alert type="success" title="Номер телефона успешно изменен" showIcon /></Form.Item> }

								<Form.Item>
									<Button type="submit" theme="success" disabled={ pristine || submitting }>Сохранить</Button>
								</Form.Item>
							</Form>
						</Col>
					</Row>
				) }
			/>
		</Fragment>
	)
}

const mapStateToProps = (state) => ({
	currentPhone: getUserPhone(state)
})

const mapDispatchToProps = (dispatch) => ({
	submitForm: (values, formApi, setError) => dispatch(changeUserPhone(values, formApi, setError))
})

export default connect(mapStateToProps, mapDispatchToProps)(ChangePhoneUser)
