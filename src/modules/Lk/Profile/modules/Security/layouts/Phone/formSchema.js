import { object, string } from 'yup'

export function getFormSchema(value) {
	return object().shape({
		phone: string()
			.min(7)
			.max(15)
			.required(),

		oldPassword: string()
			.required()
	})
}
