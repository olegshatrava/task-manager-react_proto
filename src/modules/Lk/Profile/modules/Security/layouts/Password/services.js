// @flow
import { axios, paths } from 'helpers/api'
import type { PasswordFormProps } from './types'
import getErrorsFromApi from 'utils/formatted/getErrorsFromApi'

export const requestChangePassword = async(values: PasswordFormProps, formApi: Object, setError: any): Promise<void> => {
	try {
		await axios.post(paths.userSecurity, {
			old_password: values.oldPassword,
			password: values.newPassword,
			password_confirmation: values.confirmPassword
		})

		await formApi.reset()
		setError()
	} catch (err) {
		const { errors } = err.response.data

		if (errors) {
			setError(getErrorsFromApi(err.response.data.errors))
		}
	}
}
