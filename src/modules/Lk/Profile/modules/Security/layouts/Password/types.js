// @flow

export type PasswordFormProps = {|
	oldPassword: string,
	newPassword: string,
	confirmPassword: string
|}
