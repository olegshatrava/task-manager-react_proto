import { object, string } from 'yup'

export function getFormSchema(value) {
	return object().shape({
		oldPassword: string()
			.max(255)
			.required(),

		newPassword: string()
			.min(9)
			.max(255)
			.required(),

		confirmPassword: string()
			.min(9)
			.max(255)
			.test('newPassword', 'Пароли не совпадают', function(value) {
				return value === this.parent.newPassword
			})
			.required()
	})
}
