// @flow
import React, { Fragment } from 'react'
import type { PasswordFormProps } from './types'
import { Form as FinalForm, Field } from 'react-final-form'
import { Form } from 'antd'
import { Input, Button, Alert, Row, Col, Heading, Paragraph } from 'ui/components'

import { validation } from 'helpers/formValidation'
import { getFormSchema } from './formSchema'
import { requestChangePassword } from './services'

const initialValues: PasswordFormProps = {
	oldPassword: '',
	newPassword: '',
	confirmPassword: ''
}

const submitForm = (values, formApi, setError) => requestChangePassword(values, formApi, setError)

const ChangePasswordUser = () => (
	<Fragment>
		<Heading type="h3">Смена пароля</Heading>

		<Paragraph>Вы можете использовать латинские буквы и цифры. Пароль должен быть не короче 9 символов. Не рекомендуем использовать пароли от других сайтов или слова, которые злоумышленники могут подобрать.</Paragraph>

		<FinalForm
			onSubmit={ submitForm }
			validate={ validation(getFormSchema) }
			initialValues={ initialValues }
			render={ ({ submitting, pristine, submitSucceeded, dirtySinceLastSubmit, handleSubmit, ...rest }) => (
				<Row>
					<Col md={ 4 }>
						<Form onSubmit={ handleSubmit } noValidate>
							<Form.Item>
								<Field
									type="password"
									name="oldPassword"
									placeholder="Старый пароль"
									component={ Input }
								/>
							</Form.Item>

							<Form.Item>
								<Field
									type="password"
									name="newPassword"
									placeholder="Новый пароль"
									component={ Input }
								/>
							</Form.Item>

							<Form.Item>
								<Field
									type="password"
									name="confirmPassword"
									placeholder="Повтор пароля"
									component={ Input }
								/>
							</Form.Item>

							{ (!!submitSucceeded && !dirtySinceLastSubmit) && <Form.Item><Alert type="success" title="Пароль успешно изменен" showIcon /></Form.Item> }

							<Form.Item>
								<Button type="submit" theme="success" disabled={ submitting || pristine }>Сохранить</Button>
							</Form.Item>
						</Form>
					</Col>
				</Row>
			) }
		/>
	</Fragment>
)

export default ChangePasswordUser
