import React, { Fragment } from 'react'
import { connect } from 'react-redux'
import { Form as FinalForm, Field } from 'react-final-form'
import { Form } from 'antd'
import { Input, Button, Alert, Row, Col, Heading, Paragraph } from 'ui/components'

import { getUserEmail } from '../../../../selectors/userInfo'
import { changeUserEmail } from 'modules/Lk/Profile/actions/changeUserEmail'

import { validation } from 'helpers/formValidation'
import { getFormSchema } from './formSchema'

const ChangeEmailUser = ({ currentEmail, submitForm }) => {
	const initialValues = {
		email: currentEmail.value,
		old_password: ''
	}

	return (
		<Fragment>
			<Heading type="h3">Изменить электронный адрес</Heading>

			<Paragraph>Указанная электронная почта используется для входа в систему, восстановления пароля и рассылки информации об обновлениях.</Paragraph>

			<FinalForm
				onSubmit={ submitForm }
				validate={ validation(getFormSchema) }
				initialValues={ initialValues }
				render={ ({ pristine, submitting, submitSucceeded, dirtySinceLastSubmit, handleSubmit }) => (
					<Row>
						<Col md={ 4 }>
							<Form onSubmit={ handleSubmit } noValidate>

								<Form.Item>
									<Field
										type="email"
										name="email"
										placeholder="Электронный адрес"
										component={ Input }
									/>
								</Form.Item>

								{ !pristine && (
									<Form.Item>
										<Field
											type="password"
											name="oldPassword"
											placeholder="Текущий пароль"
											component={ Input }
										/>
									</Form.Item>
								) }

								{ (!!submitSucceeded && !dirtySinceLastSubmit) && <Form.Item><Alert type="success" title="Электронный адрес успешно изменен" showIcon /></Form.Item> }

								<Form.Item>
									<Button type="submit" theme="success" disabled={ pristine || submitting }>Сохранить</Button>
								</Form.Item>
							</Form>
						</Col>
					</Row>
				) }
			/>
		</Fragment>
	)
}

const mapStateToProps = (state) => ({
	currentEmail: getUserEmail(state)
})

const mapDispatchToProps = (dispatch) => ({
	submitForm: (values, formApi, setError) => dispatch(changeUserEmail(values, formApi, setError))
})

export default connect(mapStateToProps, mapDispatchToProps)(ChangeEmailUser)
