import { object, string } from 'yup'

export function getFormSchema(value) {
	return object().shape({
		email: string()
			.email()
			.max(255)
			.required(),

		oldPassword: string()
			.required()
	})
}
