import React from 'react'
import { withRouter } from 'react-router-dom'

import { NavMenu } from '../../components'

import styles from './Sidebar.scss'

export const Sidebar = ({ match }) => (
	<aside className={ styles['profile-aside'] }>

		<NavMenu className={ styles['profile-aside__nav'] }>
			<NavMenu.Item exact to={ `${ match.path }` }>Основная информация</NavMenu.Item>
			<NavMenu.Item to={ `${ match.path }/security` }>Безопасность</NavMenu.Item>
			<NavMenu.Item to={ `${ match.path }/notification` } disabled>Уведомления</NavMenu.Item>
		</NavMenu>

	</aside>
)

export default withRouter(Sidebar)
