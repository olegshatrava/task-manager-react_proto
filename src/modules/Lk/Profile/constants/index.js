// @flow

// Get info
export const FETCH_USERINFO_LOADING: string = 'userInfo/FETCH_LOADING'
export const FETCH_USERINFO_SUCCESS: string = 'userInfo/FETCH_SUCCESS'
export const FETCH_USERINFO_FAILED: string = 'userInfo/FETCH_FAILED'

// Change Info
export const EDIT_BASICINFO_SUCCESS: string = 'userInfo/basic/EDIT_SUCCESS'

// Change phone
export const CHANGE_USERPHONE_SUCCESS: string = 'userInfo/phone/CHANGE_SUCCESS'

// Change email
export const CHANGE_USEREMAIL_SUCCESS: string = 'userInfo/email/CHANGE_SUCCESS'

// Change photo
export const CHANGE_USER_PHOTO_SUCCESS: string = 'userInfo/photo/CHANGE_SUCCESS'
