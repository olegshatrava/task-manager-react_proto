// @flow
import { axios, paths } from 'helpers/api'
import getErrorsFromApi from 'utils/formatted/getErrorsFromApi'

import * as types from '../constants'

export type UserData = {|
	email: string,
	oldPassword: string
|}

/**
* HTTP Request for change user email
*/
export const changeUserEmail = (userData: UserData, formApi: Object, setError: Function) => async(dispatch: Function): Promise<void> => {
	const requestData = {
		email: userData.email
	}

	try {
		await axios.post(paths.userSecurity, { ...requestData, old_password: userData.oldPassword })
		await dispatch(changeUserEmailSuccess(requestData))
		setError()
	} catch (err) {
		const { errors } = err.response.data

		if (errors) {
			setError(getErrorsFromApi(err.response.data.errors))
		}
	}
}

export const changeUserEmailSuccess = (data: Object) => ({
	type: types.CHANGE_USEREMAIL_SUCCESS,
	payload: data
})
