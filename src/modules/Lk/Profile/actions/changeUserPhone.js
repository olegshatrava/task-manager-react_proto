// @flow
import { axios, paths } from 'helpers/api'
import getErrorsFromApi from 'utils/formatted/getErrorsFromApi'

import * as types from '../constants'

export type UserData = {|
	phone: string,
	oldPassword: string
|}

/**
* HTTP Request for change user phone
*/
export const changeUserPhone = (userData: UserData, formApi: Object, setError: Function) => async(dispatch: Function): Promise<void> => {
	const requestData = {
		phone: userData.phone
	}

	try {
		await axios.post(paths.userSecurity, { ...requestData, old_password: userData.oldPassword })
		await dispatch(changeUserPhoneSuccess(requestData))
		setError()
	} catch (err) {
		const { errors } = err.response.data

		if (errors) {
			setError(getErrorsFromApi(err.response.data.errors))
		}
	}
}

export const changeUserPhoneSuccess = (data: Object) => ({
	type: types.CHANGE_USERPHONE_SUCCESS,
	payload: data
})
