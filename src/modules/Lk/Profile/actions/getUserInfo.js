// @flow
import { axios, paths } from 'helpers/api'
import { replace } from 'react-router-redux'

import * as types from '../constants'

/**
* HTTP Request for user info
*/
export const getUserInfo = () => async(dispatch: Function): Promise<void> => {
	await dispatch(userInfoRequest())

	try {
		const { data } = await axios.get(paths.userInfo)
		await dispatch(userInfoSuccess(data.data))
	} catch (err) {
		dispatch(replace('/logout'))
		dispatch(userInfoFailed(err.response.data.message))
	}
}

export const userInfoRequest = () => ({
	type: types.FETCH_USERINFO_LOADING
})

export const userInfoSuccess = (data: Object) => ({
	type: types.FETCH_USERINFO_SUCCESS,
	payload: data
})

export const userInfoFailed = (errors: Object) => ({
	type: types.FETCH_USERINFO_FAILED,
	errors
})
