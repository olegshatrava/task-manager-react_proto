// @flow
import { axios, paths } from 'helpers/api'

import * as types from '../constants'

export type UserData = {|
	lastName: string,
	firstName: string,
	middleName: string,
	username: string,
	birthday: string
|}

/**
* HTTP Request for user info
*/
export const editUserInfo = (userData: UserData, formApi: Object, setError: Function) => async(dispatch: Function): Promise<void> => {
	const requestData = {
		last_name: userData.lastName,
		first_name: userData.firstName,
		middle_name: userData.middleName,
		username: userData.username,
		birthday: userData.birthday
	}

	try {
		await axios.post(paths.userInfo, requestData)
		await dispatch(editUserInfoSuccess(requestData))
	} catch (err) {
		setError(err.response.data.errors)
	}
}

export const editUserInfoSuccess = (data: Object) => ({
	type: types.EDIT_BASICINFO_SUCCESS,
	payload: data
})
