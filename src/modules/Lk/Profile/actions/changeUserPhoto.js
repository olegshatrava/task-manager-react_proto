// @flow
import { axios, paths } from 'helpers/api'

import * as types from '../constants'

/**
* HTTP Request for user info
*/
export const changeUserPhoto = (fd: FormData, cb: () => void) => async(dispatch: Function): Promise<void> => {
	try {
		const { data } = await axios.post(paths.changeUserPhoto, fd)
		await dispatch(changeUserPhotoSuccess(data.data))
		await cb()
	} catch (err) {
		global.console.log(err.response)
		// setError(err.response.data.errors)
	}
}

export const changeUserPhotoSuccess = (data: Object) => ({
	type: types.CHANGE_USER_PHOTO_SUCCESS,
	payload: {
		path: data.path,
		thumb: data.thumb
	}
})
