import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Switch, Route, Redirect } from 'react-router-dom'

import Sidebar from './layouts/Sidebar'

import MainInfo from './modules/MainInfo'
import Security from './modules/Security'
import Notification from './modules/Notification'

import styles from './Profile.scss'

class Profile extends Component {
	render() {
		const { match } = this.props

		return (
			<div className={ styles['container'] }>
				<Sidebar />

				<div className={ styles['content-page'] }>
					<Switch>
						<Route exact path={ `${ match.url }` } component={ MainInfo } />
						<Route path={ `${ match.url }/security` } component={ Security } />
						<Route path={ `${ match.url }/notification` } component={ Notification } />
						<Redirect to={ `${ match.url }` } />
					</Switch>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	userInfo: state.userInfo
})

export default connect(mapStateToProps)(Profile)
