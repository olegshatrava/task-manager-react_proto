// @flow
import React, { type Node } from 'react'
import classnames from 'classnames/bind'

import styles from './styles/NavMenu.scss'
const cn = classnames.bind(styles)

export type NavMenuProps = {
	className?: string,
	children: any
}

export const NavMenu = ({ className, children }: NavMenuProps): Node => (
	<nav className={ cn('profile-nav', className) }>
		<ul className={ styles['profile-nav__list'] }>
			{ children }
		</ul>
	</nav>
)

export default NavMenu
