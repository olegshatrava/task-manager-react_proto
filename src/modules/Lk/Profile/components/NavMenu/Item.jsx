// @flow
import React, { type Node } from 'react'
import { NavLink } from 'react-router-dom'
import classnames from 'classnames/bind'

import styles from './styles/NavMenu.scss'
const cn = classnames.bind(styles)

type NavLinkItemProps = {
	to: string,
	exact?: boolean,
	disabled?: boolean,
	children: string | Object
}

export const NavLinkItem = ({
	to,
	disabled,
	children,
	...restProps
}: NavLinkItemProps): Node => {
	const handleClick = (evt: SyntheticEvent<HTMLLinkElement>): void => {
		if (disabled) {
			evt.preventDefault()
		}
	}

	return (
		<li className={ styles['nav-menu__item'] }>
			<NavLink
				to={ !disabled ? to : '#' }
				{ ...restProps }
				onClick={ handleClick }
				className={ cn('nav-menu__item-link', !!disabled && 'nav-menu__item-link--disabled') }
				activeClassName={ styles['nav-menu__item-link--active'] }
			>
				{ children }
			</NavLink>
		</li>
	)
}

export default NavLinkItem
