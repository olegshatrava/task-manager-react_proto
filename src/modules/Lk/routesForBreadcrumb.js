/**
* Список роутов отображаемых в хлебных крошках.
*/

export const routesForBreadcrumb = {
	'/profile': 'Профиль',
	'/profile/security': 'Безопасность',
	'/profile/notification': 'Уведомления',
	'/tasks': 'Задачи',
	'/projects': 'OpenHub',
	'/projects/d/1': '#Разработка',
	'/c': 'Подробная информация по задачи'
}
