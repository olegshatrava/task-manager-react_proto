import React from 'react'

import NavCategory from './components/NavCategory'
import TasksList from './layouts/TasksList'

import styles from './Tasks.scss'

const TasksPage = () => (
	<div className={ styles['container'] }>
		<div className={ styles['tabs'] }>
			<NavCategory />
		</div>

		<div className={ styles['tasks-list'] }>

			<TasksList />

		</div>
	</div>
)

export default TasksPage
