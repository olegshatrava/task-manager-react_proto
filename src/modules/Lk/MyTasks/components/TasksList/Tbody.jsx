import React from 'react'

const TbodyTaskList = ({ children }) => (
	<div style={ {
		overflowX: 'auto',
		height: 'calc(100vh - 170px)'
	} }>
		{ children }
	</div>
)

export default TbodyTaskList
