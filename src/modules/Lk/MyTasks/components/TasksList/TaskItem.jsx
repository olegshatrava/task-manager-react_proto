import React from 'react'
import classnames from 'classnames/bind'
import { withRouter, Link } from 'react-router-dom'
import format from 'date-fns/format'

import { Avatar } from 'ui/components'

import styles from './TasksList.scss'
const cn = classnames.bind(styles)

const TaskItem = ({ location, info }) => {
	const creatorUserName = [info.creator.first_name, info.creator.last_name].join(' ')

	return (
		<div className={ cn('item', 'item__body') }>
			<div className={ cn('item-col', 'item-col_name') }>
				<Link
					to={ {
						pathname: `/c/${ info.id }`,
						state: { modal: true, redirectTo: `${ location.pathname }${ location.search }` }
					} }
					className={ styles['item-col_name-link'] }
				>
					<span className={ styles['item-col_name-title'] }>{ info.title }</span>
				</Link>
			</div>
			<div className={ cn('item-col', 'item-col_location') }>
				<Link to="/projects/d/1" className={ cn('label-loc') }>{ [info.project.name, info.project.department.name, info.project.department.process && info.project.department.process.name].filter(Boolean).join(' / ') }</Link>
			</div>
			<div className={ cn('item-col', 'item-col_author') }>
				<span className={ cn('author-wrap') }>
					<Avatar
						src={ info.creator.image ? info.creator.image.thumb : '//placehold.it/150x150' }
						alt={ creatorUserName }
						size="sm"
						className={ styles['author-wrap_pic'] }
					/>
					<span className={ styles['author-wrap_name'] }>{ creatorUserName }</span>
				</span>
			</div>
			<div className={ cn('item-col', 'item-col_date') }>
				<span>{ info.date ? format(info.date, 'DD.MM.YYYY') : 'Не указан' }</span>
			</div>
		</div>
	)
}

export default withRouter(TaskItem)
