import React from 'react'

import styles from './TasksList.scss'

const TheadTaskList = () => (
	<div className={ `${ styles['item'] } ${ styles['item__head'] }` }>
		<div className={ `${ styles['item-col'] } ${ styles['item-col_name'] }` }>
			<span>Название</span>
		</div>
		<div className={ `${ styles['item-col'] } ${ styles['item-col_location'] }` }>
			<span>Расположение</span>
		</div>
		<div className={ `${ styles['item-col'] } ${ styles['item-col_author'] }` }>
			<span>Автор</span>
		</div>
		<div className={ `${ styles['item-col'] } ${ styles['item-col_date'] }` }>
			<span>Срок</span>
		</div>
	</div>
)

export default TheadTaskList
