import React from 'react'
import { withRouter, NavLink } from 'react-router-dom'
import qs from 'query-string'

import styles from './NavCategory.scss'

const hasNameSection = (name) => (match, location) => {
	const currrentSection = qs.parse(location.search)['section']

	return currrentSection === name
}

const getRoutes = (match) => [
	{
		to: match.url,
		exact: true,
		name: 'Все задачи',
		isActive: hasNameSection(),
		className: styles['category-list_link'],
		activeClassName: styles['category-list_link__active']
	}, {
		to: `${ match.url }?section=input`,
		name: 'Входящие',
		isActive: hasNameSection('input'),
		className: styles['category-list_link'],
		activeClassName: styles['category-list_link__active']
	}, {
		to: `${ match.url }?section=output`,
		name: 'Исходящие',
		isActive: hasNameSection('output'),
		className: styles['category-list_link'],
		activeClassName: styles['category-list_link__active']
	}, {
		to: `${ match.url }?section=process`,
		name: 'В процессе',
		isActive: hasNameSection('process'),
		className: styles['category-list_link'],
		activeClassName: styles['category-list_link__active']
	}, {
		to: `${ match.url }?section=finish`,
		name: 'Выполненные',
		isActive: hasNameSection('finish'),
		className: styles['category-list_link'],
		activeClassName: styles['category-list_link__active']
	}
]

const NavCategory = ({ match }) => {
	const routes = getRoutes(match)

	return (
		<ul className={ styles['category-list'] }>
			{ routes.map(({ name, ...params }, idx) => (
				<li key={ idx } className={ styles['category-list_item'] }>
					<NavLink { ...params }>{ name }</NavLink>
				</li>
			)) }
		</ul>
	)
}

export default withRouter(NavCategory)
