/* eslint arrow-body-style: 0 */
import React, { Component, Fragment } from 'react'
import { withRouter } from 'react-router-dom'
import qs from 'query-string'
import { axios, paths } from 'helpers/api'

import { Alert, Preloader } from 'ui/components'
import { Thead, Tbody } from '../../components/TasksList'

const states = {
	loading: 'loading',
	load: 'load',
	error: 'error'
}

const ConnectStateMyTasks = (ChildComponent) => {
	class LoadingMyTasks extends Component {
		__isMounted = false

		state = {
			hasState: null,
			data: null,
			errorMessage: null
		}

		componentDidMount() {
			this._isMounted = true

			const currentSection = this.checkCurrentSection()
			this.requestTasks(currentSection)
		}

		componentDidUpdate(nextProps) {
			if (nextProps.location.search !== this.props.location.search) {
				const currentSection = this.checkCurrentSection()
				this.requestTasks(currentSection)
			}
		}

		componentWillUnmount() {
			this._isMounted = false
		}

		checkCurrentSection = () => {
			const searchParams = qs.parse(this.props.location.search)

			if (searchParams['section'] !== undefined) {
				return searchParams['section']
			}

			return null
		}

		requestTasks = async(section) => {
			this.setState({ hasState: states.loading })

			try {
				const { data } = await axios.get(paths.tasks, {
					params: { status: section }
				})

				this._isMounted && this.setState({ hasState: states.load, data: data.data })
			} catch (err) {
				const { data } = err.response
				this._isMounted && this.setState({ hasState: states.error, errorMessage: data.message })
			}
		}

		render() {
			const { hasState, data, errorMessage } = this.state

			let nextChildren = null

			if (hasState === states.loading) {
				nextChildren = <Preloader />
			}

			if (hasState === states.error) {
				nextChildren = (
					<Alert
						type="error"
						title="Что-то пошло не так :("
						description={ errorMessage || 'Произошла ошибка, попробуйте позже.' }
						style={ { margin: '25px' } }
						showIcon
					/>
				)
			}

			if (hasState === states.load) {
				nextChildren = <ChildComponent list={ data } />
			}

			return (
				<Fragment>
					<Thead />

					<Tbody>

						{ nextChildren }

					</Tbody>
				</Fragment>
			)
		}
	}

	return withRouter(LoadingMyTasks)
}

export default ConnectStateMyTasks
