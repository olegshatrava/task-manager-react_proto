import React from 'react'
import { compose } from 'recompose'

import { TaskItem } from '../../components/TasksList'
import ConnectState from './ConnectState'

const styles = {
	display: 'flex',
	justifyContent: 'center',
	padding: '30px 0',
	fontSize: '14px'
}

const TasksList = ({ list }) => {
	if (list && list.length === 0) {
		return <div style={ styles }>Список задач пуст.</div>
	}

	return list && list.map((item, idx) => <TaskItem key={ item.id } info={ item } />)
}

export default compose(
	ConnectState
)(TasksList)
