import { combineReducers } from 'redux'

import { isOpenSidebar } from './isOpenSidebar'

export const sidebar = combineReducers({
	isOpenSidebar
})
