// @flow
import * as types from '../constants'

export type ActionType = {|
	type: string,
	isOpen?: boolean
|}

export const isOpenSidebar = (state: boolean = false, action: ActionType): boolean => {
    switch (action.type) {
		case types.TOGGLE_SIDEBAR: {
			const { isOpen } = action

			if (typeof isOpen === 'boolean') {
				return isOpen
			}

			return !state
		}

		case '@@router/LOCATION_CHANGE': {
			return false
		}

        default:
            return state
    }
}
