import React from 'react'
import classnames from 'classnames/bind'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import Menu from './components/Menu'
import Logo from './components/Logo'
import ButtonToggle from './components/ButtonToggle'

import { toggleSidebar } from './actions'
import { isOpenSidebar } from './selectors/sidebar'

import styles from './Sidebar.scss'
const cn = classnames.bind(styles)

const Sidebar = ({ match, toggleSidebar, isOpenSidebar }) => (
	<aside className={ cn('container', !!isOpenSidebar && ['container--is-open']) }>

		<div className={ styles['logo'] }>
			<Logo isOpenSidebar={ isOpenSidebar } to="/" />

			{ !!isOpenSidebar && (
				<ButtonToggle
					theme="purple"
					isOpen={ isOpenSidebar }
					onClick={ toggleSidebar }
				/>
			) }
		</div>

		<Menu isOpenSidebar={ isOpenSidebar }>
			<Menu.Item to={ { pathname: `/c/new`, state: { modal: true, redirectTo: `${ location.pathname }${ location.search }` } } } icon={ ['far', 'plus'] }>Создать задачу</Menu.Item>
			<Menu.Divider />
			<Menu.Item count="11" to="/tasks" icon="list-ul">Задачи</Menu.Item>
			<Menu.Item disabled icon="lightbulb">Идеи</Menu.Item>
			<Menu.Item disabled icon="comment">Сообщения</Menu.Item>
			<Menu.Divider />
			<Menu.Item disabled icon="star">Избранное</Menu.Item>
			<Menu.Item disabled icon="calendar-alt">Календарь</Menu.Item>
			<Menu.Divider />
			<Menu.Item submenu icon="layer-group">Проекты</Menu.Item>
		</Menu>

	</aside>
)

const mapStateToProps = (state) => ({
	isOpenSidebar: isOpenSidebar(state)
})

const mapDispatchToProps = dispatch => ({
	toggleSidebar: () => dispatch(toggleSidebar())
})

export default compose(
	withRouter,
	connect(mapStateToProps, mapDispatchToProps)
)(Sidebar)
