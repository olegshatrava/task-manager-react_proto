// @flow

import * as types from '../constants'

export const toggleSidebar = (isOpen: boolean | null = null) => ({
	type: types.TOGGLE_SIDEBAR,
	isOpen
})
