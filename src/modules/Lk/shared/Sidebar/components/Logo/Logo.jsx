// @flow
import React, { type Node } from 'react'
import classnames from 'classnames/bind'

import { Link } from 'react-router-dom'

import LogoSmall from './LogoSmall'
import LogoText from './LogoText'

import styles from './Logo.scss'
const cn = classnames.bind(styles)

export type LogoProps = {|
	to: string,
	isOpenSidebar: boolean
|}

const Logo = ({
	to,
	isOpenSidebar
}: LogoProps): Node => (
	<Link to={ to } className={ cn('container') }>
		<LogoSmall />
		{isOpenSidebar &&
			<LogoText />
		}
	</Link>
)

export default Logo
