// @flow
import React, { type Node } from 'react'
import { NavLink } from 'react-router-dom'
import { compose, withHandlers } from 'recompose'
import classnames from 'classnames/bind'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import Badge from 'ui/components/Badge'

import styles from './Menu.scss'
const cn = classnames.bind(styles)

export type MenuProps = {
	to?: string,
	icon: string | ['fab' | 'far' | 'fas', string],
	count?: number,
	submenu: boolean,
	disabled?: boolean,
	isOpenSidebar?: boolean,
	children: string,
	onClick: (evt: SyntheticEvent<HTMLButtonElement>) => void
}

const MenuItem = ({
	to,
	icon,
	count,
	submenu,
	disabled,
	isOpenSidebar,
	onClick,
	children,
	...restParams
}: MenuProps): Node => (
	<li className={ styles['item'] }>
		<NavLink
			to={ to || '#' }
			className={ cn('item-link', (!!disabled) && 'item-link__disabled') }
			activeClassName={ styles['item-link__active'] }
			onClick={ onClick }
			{ ...restParams }
		>
			<div className={ cn('item-icon') } title={ children }>
				<FontAwesomeIcon icon={ icon } />
			</div>

			<div className={ cn('item-text') }>
				{ !!isOpenSidebar && <span>{ children }</span> }
			</div>

			{ !!count && (
				<Badge
					className={ !isOpenSidebar && cn('item-badge-doted') }
					value={ count }
					maxValue="99"
					theme="warning"
					size={ isOpenSidebar ? 'md' : 'sm' }
					isDoted={ !isOpenSidebar }
				/>
			) }
		</NavLink>
	</li>
)

export default compose(
	withHandlers({
		onClick: props => evt => {
			if (props.disabled || !props.to) {
				evt.preventDefault()
			}
			if (props.onClick) {
				props.onClick()
			}
		}
	})
)(MenuItem)
