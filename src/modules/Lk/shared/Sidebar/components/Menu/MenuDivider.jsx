// @flow
import React, { type Node } from 'react'

import Divider from 'ui/components/Divider'

import styles from './Menu.scss'

const MenuDivider = (): Node => (
	<li>
		<Divider type="horizontal" theme="light-blue" className={ styles['divider'] } />
	</li>
)

export default MenuDivider
