import React, { Component, Fragment } from 'react'

import Submenu from './Submenu'

import styles from './Menu.scss'

class Menu extends Component {
	state = {
		isSubmenuOpen: false
	}

	handleOpenSubmenu = () => this.setState({ isSubmenuOpen: true })
	handleCloseSubmenu = () => this.setState({ isSubmenuOpen: false })

	render() {
		const { children, isOpenSidebar } = this.props

		return (
			<Fragment>
				<ul className={ styles['sidebar-menu'] }>
					{ React.Children.map(children, child => {
						if (child.props.submenu) {
							return React.cloneElement(child, {
								isOpenSidebar,
								onClick: this.handleOpenSubmenu
							})
						}
						return React.cloneElement(child, {
							isOpenSidebar
						})
					}) }
				</ul>

				<Submenu
					isOpen={ this.state.isSubmenuOpen }
					handleToggle={ this.handleCloseSubmenu }
				/>
			</Fragment>
		)
	}
}

export default Menu
