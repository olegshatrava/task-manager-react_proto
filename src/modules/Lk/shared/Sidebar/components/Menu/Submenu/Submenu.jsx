import React, { Component } from 'react'

import Button from 'ui/components/Button'
import SubmenuItem from './SubmenuItem'

import classnames from 'classnames/bind'
import styles from '../Menu.scss'
const cn = classnames.bind(styles)

class Submenu extends Component {
	state = {
		projects: [
			{
				name: 'OpenHub',
				boards: [
					{
						board: 'Разработка',
						to: '/projects/d/1'
					},
					{
						board: 'Дизайн',
						to: '/projects/d/1'
					}]
			},
			{
				name: 'Openh',
				boards: [
					{
						board: 'Разработка',
						to: '/projects/d/1'
					},
					{
						board: 'Дизайн',
						to: '/projects/d/1'
					},
					{
						board: 'Реклама',
						to: '/projects/d/1'
					},
					{
						board: 'Юристы',
						to: '/projects/d/1'
					}]
			},
			{
				name: 'Openi',
				boards: [
					{
						board: 'Разработка',
						to: '/projects/d/1'
					},
					{
						board: 'Дизайн',
						to: '/projects/d/1'
					},
					{
						board: 'Реклама',
						to: '/projects/d/1'
					},
					{
						board: 'Юристы',
						to: '/projects/d/1'
					}]
			}
		]
	}

	handleAction = (evt) => evt.stopPropagation()

	render() {
		const { isOpen, handleToggle } = this.props

		return (
			<div className={ cn('submenu', isOpen && ['submenu__is-open']) } onClick={ handleToggle }>
				<div className={ cn('submenu-container') } onClick={ this.handleAction }>
					<div className={ cn('submenu-header') }>
						<Button
							size="lg"
							icon={ ['fas', 'arrow-left'] }
							onClick={ handleToggle }
							transparent
						>
							Проекты
						</Button>
					</div>

					<ul className={ cn('submenu-list') }>
						{ this.state.projects.map(item => (
							<SubmenuItem
								key={ item.name }
								name={ item.name }
								boards={ item.boards }
								onCloseMenu={ handleToggle }
							/>
						)) }
					</ul>
				</div>
			</div>
		)
	}
}

export default Submenu
