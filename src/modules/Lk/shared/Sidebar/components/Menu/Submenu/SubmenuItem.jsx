import React, { Component } from 'react'

import { NavLink } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import classnames from 'classnames/bind'
import styles from '../Menu.scss'
const cn = classnames.bind(styles)

class SubmenuItem extends Component {
	state = {
		isOpenDropdown: false
	}

	handleToggleDropdown = () => {
		this.setState({ isOpenDropdown: !this.state.isOpenDropdown })
	}

	render() {
		const { isOpenDropdown } = this.state
		const { name, boards, onCloseMenu } = this.props

		return (
			<li key={ name } className={ cn('submenu-item', isOpenDropdown && 'submenu-item_open') }>
				<div className={ cn('submenu-item-title') } onClick={ this.handleToggleDropdown }>
					<span>{ name }</span>
					<FontAwesomeIcon icon={ isOpenDropdown ? 'angle-up' : 'angle-down' } />
				</div>

				<div className={ cn('submenu-item-boards') }>
					{ boards.map(board => (
						<NavLink
							key={ board.board }
							to={ board.to || '#' }
							className={ cn('submenu-item-board') }
							onClick={ onCloseMenu }>
							{ board.board }
						</NavLink>
					)) }
				</div>
			</li>
		)
	}
}

export default SubmenuItem
