// @flow
import React, { type Node } from 'react'
import Button, { type ButtonThemes } from 'ui/components/Button'
import classnames from 'classnames/bind'

import styles from './ButtonToggle.scss'
const cn = classnames.bind(styles)

type ButtonToggle = {|
	theme?: ButtonThemes,
	isOpen: boolean,
	className?: string,
	onClick: (value?: boolean) => void
|}

const ButtonToggleSidebar = ({ isOpen, className, onClick, ...restProps }: ButtonToggle): Node => {
	const classesIcon: string = !isOpen ? 'fa-rotate-180' : ''

	return (
		<Button
			shape="circle"
			size="lg"
			icon={ ['far', 'stream'] }
			onClick={ onClick }
			className={ cn('icon', !isOpen && 'is-closed', className) }
			classNameIcon={ classesIcon }
			transparent
			{ ...restProps }
		/>
	)
}

export default ButtonToggleSidebar
