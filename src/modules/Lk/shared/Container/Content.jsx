import React from 'react'

import styles from './Container.scss'

const ContainerContent = (props) => (
    <div className={ styles['content'] }>
        { props.children }
    </div>
)

export default ContainerContent
