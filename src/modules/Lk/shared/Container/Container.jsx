import React from 'react'

import styles from './Container.scss'

const Container = (props) => (
    <div className={ styles['container'] }>
        { props.children }
    </div>
)

export default Container
