import React from 'react'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import { withRouter } from 'react-router-dom'
import { Breadcrumb } from 'ui/components'

import { routesForBreadcrumb } from 'modules/Lk/routesForBreadcrumb'
import { isOpenSidebar } from 'modules/Lk/shared/Sidebar/selectors/sidebar'

import ButtonToggle from '../Sidebar/components/ButtonToggle'
import { toggleSidebar } from '../Sidebar/actions'
import AuthUser from './AuthUser'

import styles from './Header.scss'

const HeaderLk = ({ userBasicInfo, metaInfo, toggleSidebar, isOpenSidebar }) => (
	<header className={ styles['container'] }>

		{ !isOpenSidebar && (
			<ButtonToggle
				isOpen={ isOpenSidebar }
				onClick={ toggleSidebar }
			/>
		) }

		<div className={ styles['container-breadcrumb'] }>
			<Breadcrumb routes={ routesForBreadcrumb } />
		</div>

		<AuthUser />
	</header>
)

const mapStateToProps = (state) => ({
	isOpenSidebar: isOpenSidebar(state)
})

const mapDispatchToProps = dispatch => ({
	toggleSidebar: () => dispatch(toggleSidebar())
})

export default compose(
	withRouter,
	connect(mapStateToProps, mapDispatchToProps)
)(HeaderLk)
