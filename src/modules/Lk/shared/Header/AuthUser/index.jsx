import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import Avatar from 'ui/components/Avatar'
import Dropdown from 'ui/components/Dropdown'

import { getUserBasicInfo, getUserPhoto, metaUserInfoRequest } from 'modules/Lk/Profile/selectors/userInfo'

import styles from './AuthUser.scss'

class AuthUser extends Component {
	render() {
		const { basicInfo, photoUser, metaInfo } = this.props

		if (!metaInfo.isLoad) {
			return 'Загрузка...'
		}

		const fullUserName = [basicInfo.firstName, basicInfo.lastName].join(' ')

		const menu = (
			<Dropdown.Menu>
				<Dropdown.MenuItem>
					<Link to="/profile">Профиль</Link>
				</Dropdown.MenuItem>

				<Dropdown.MenuItem>
					<Link to="/logout">Выход</Link>
				</Dropdown.MenuItem>
			</Dropdown.Menu>
		)

		return (
			<Dropdown
				overlay={ menu }
			>
				<div className={ styles['container'] }>
					<Avatar
						src={ photoUser.thumb }
						alt={ fullUserName }
						size="sm"
						className={ styles['avatar'] }
					/>

					<span className={ styles['username'] }>{ fullUserName }</span>
				</div>
			</Dropdown>
		)
	}
}

const mapStateToProps = (state) => ({
	basicInfo: getUserBasicInfo(state),
	photoUser: getUserPhoto(state),
	metaInfo: metaUserInfoRequest(state)
})

export default connect(mapStateToProps)(AuthUser)
