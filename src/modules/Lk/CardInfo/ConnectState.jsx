/* eslint arrow-body-style: 0 */
import React, { Component, Fragment } from 'react'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import { axios, paths } from 'helpers/api'
import { Preloader, Alert, Button } from 'ui/components'

import Container from './components/Container'
import ActionsBar from './components/ActionsBar'

import { createdTask, editTask } from './actions'

const states = {
	created: 'created',
	loading: 'loading',
	load: 'load',
	error: 'error'
}

const ConnectStateCardInfo = (ChildComponent) => {
	class LoadingCardInfo extends Component {
		_isMounted = false

		state = {
			hasState: null,
			data: null,
			errors: null
		}

		componentDidMount() {
			this._isMounted = true
			const { params } = this.props.match

			// Check for available task
			if (params && params.cardId && !isNaN(params.cardId)) {
				return this.requestInfoTask(params.cardId)
			}

			// If dont available task to new created mod
			this.setState({ hasState: states.created })
		}

		componentWillUnmount() {
			this._isMounted = false
		}

		requestInfoTask = async(cardId) => {
			this.setState({ hasState: states.loading })

			try {
				const { data } = await axios.get(`${ paths.tasks }/${ cardId }`)
				!!this._isMounted && this.setState({ hasState: states.load, data: data.data })
			} catch (err) {
				const { data } = err.response
				!!this._isMounted && this.setState({ hasState: states.error, errors: data.message })
			}
		}

		// acceptTask = async(cardId) => {
		// 	try {
		// 		await axios.get(`${ paths.tasks }/${ cardId }/accept`)
		// 		console.log(data)
		// 	} catch (err) {
		// 		console.log(err)
		// 	}
		// }

		render() {
			const { hasState, data, errors } = this.state
			const { location, createdTask, editTask } = this.props

			let nextChildren = null
			let actionBarChldren = null

			/**
			* Preloader loading task info
			*/
			if (hasState === states.loading) {
				nextChildren = <Preloader size="lg" />
			}

			/**
			* Render errors in get task info
			*/
			if (hasState === states.error) {
				nextChildren = (
					<Alert
						type="error"
						title="Что-то пошло не так :("
						description={ errors || 'Произошла ошибка, попробуйте позже.' }
						showIcon
					/>
				)
			}

			/**
			* Render taks info or card for created
			*/
			if (hasState === states.load || hasState === states.created) {
				const { params } = this.props.match
				const isCreated = isNaN(params.cardId)

				nextChildren = (
					<ChildComponent
						isCreated={ isCreated }
						cardId={ params.cardId || null }
						info={ data ? data.task : {} }
						users={ data ? data.users : [] }
						submitForm={ isCreated ? createdTask : editTask }
					/>
				)

				if (!isCreated && data.task.status === 1) {
					actionBarChldren = (
						<Fragment>
							<Button theme="purple" icon={ ['far', 'check'] } onClick={ () => console.log('Accept') } title="Подтвердить">Подтвердить</Button>
							<Button theme="danger" icon={ ['far', 'ban'] } onClick={ () => console.log('Cancel') } title="Отказаться">Отклонить</Button>
						</Fragment>
					)
				}
			}

			const prevPath = location.state && location.state.redirectTo || '/'

			return (
				<Container to={ prevPath } position="right">
					<ActionsBar>
						{ actionBarChldren }
						<Button disabled theme="default" icon={ ['far', 'ellipsis-h'] } title="Дополнительно" />
						<Button to={ prevPath } theme="default" icon={ ['far', 'times'] } title="Закрыть" />
					</ActionsBar>

					<div style={ { overflowX: 'auto', height: '100%' } }>
						{ nextChildren }
					</div>
				</Container>
			)
		}
	}

	const mapDispatchToProps = (dispatch) => ({
		createdTask: (values, formApi) => dispatch(createdTask(values, formApi)),
		editTask: (values, formApi) => dispatch(editTask(values, formApi))
	})

	return compose(
		withRouter,
		connect(null, mapDispatchToProps)
	)(LoadingCardInfo)
}

export default ConnectStateCardInfo
