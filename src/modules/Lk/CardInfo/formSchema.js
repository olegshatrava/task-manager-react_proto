import { object, string } from 'yup'

export function getFormSchema(value) {
	return object().shape({
		title: string().required(),
		description: string()
	})
}
