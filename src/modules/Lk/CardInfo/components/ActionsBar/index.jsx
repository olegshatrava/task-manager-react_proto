import React from 'react'

import styles from './ActionsBar.scss'

const ActionsBar = ({ children }) => (
	<div className={ styles['wrapper'] }>
		{ children }
	</div>
)

export default ActionsBar
