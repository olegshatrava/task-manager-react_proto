// @flow
import React from 'react'

import { Button, Avatar } from 'ui/components'

import styles from './UsersList.scss'

type UserInfo = {
	id: number,
	path: string,
	first_name?: string,
	last_name?: string
}

type UsersListProps = {
	title: string,
	users?: UserInfo[]
}

const UsersList = ({ title, users = [] }: UsersListProps) => (
	<div className={ styles['users-list-wrapper'] }>
		<span className={ styles['users-list-title'] }>{ title }</span>

		<div className={ styles['users-list-users'] }>
			{ users && users.map((user, idx) => (
				<Avatar
					key={ user.id }
					src={ 'https://devapihub.openi.ru/storage/app/public/thumb_avatars/d9/32/e8cdbed15b91a425ead7e6db5f04.jpg' }
					alt={ [user.first_name, user.last_name].join(' ') }
					size="sm"
				/>
			)) }

			<Button theme="default" size="sm" icon={ ['far', 'plus'] } />
		</div>
	</div>
)

export default UsersList
