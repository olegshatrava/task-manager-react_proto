import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'

import styles from './ContainerAbsolute.scss'

class ContainerAbsolute extends Component {
	handleClickOutside = () => this.props.dispatch(push(this.props.to))

	handleClickCard = (evt) => evt.stopPropagation()

	render() {
		const { children, position } = this.props

		return (
			<div className={ `${ styles['wrapper'] } ${ styles[`wrapper-content__${ position }`] }` } onClick={ this.handleClickOutside }>
				<div className={ styles['container'] } onClick={ this.handleClickCard }>
					{ children }
				</div>
			</div>
		)
	}
}

export default connect()(ContainerAbsolute)
