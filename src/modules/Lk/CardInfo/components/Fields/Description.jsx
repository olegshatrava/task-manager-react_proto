import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import { Textarea } from 'ui/components'

import styles from './FieldsTask.scss'

class DescriptionTask extends Component {
	onBlur = (evt) => {
		this.props.input.onBlur(evt)
		this.props.handleSubmit()
	}

	render() {
		const { input, meta, children, render, handleSubmit, ...restProps } = this.props

		return (
			<div className={ styles['desc-wrapper'] }>
				<span className={ styles['desc_icon'] }>
					<FontAwesomeIcon icon={ ['fal', 'align-left'] } />
				</span>

				<Textarea
					name={ input.name }
					defaultValue={ input.value }
					size="md"
					minRows={ !meta.active ? 1 : 5 }
					maxRows={ 15 }
					className={ styles['field'] }
					onFocus={ input.onFocus }
					onBlur={ this.onBlur }
					onChange={ input.onChange }
					{ ...restProps }
				/>
			</div>
		)
	}
}

export default DescriptionTask
