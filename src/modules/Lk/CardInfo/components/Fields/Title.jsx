import React, { Component } from 'react'

import { Textarea } from 'ui/components'

import styles from './FieldsTask.scss'

class TitleTask extends Component {
	onBlur = (evt) => {
		this.props.input.onBlur(evt)
		this.props.handleSubmit()
	}

	render() {
		const { input, meta, children, render, handleSubmit, ...restProps } = this.props

		return (
			<Textarea
				name={ input.title }
				defaultValue={ input.value }
				size="lg"
				minRows={ 1 }
				className={ `${ styles['field'] } ${ styles['field_title'] }` }
				onFocus={ input.onFocus }
				onBlur={ this.onBlur }
				onChange={ input.onChange }
				{ ...restProps }
			/>
		)
	}
}

export default TitleTask
