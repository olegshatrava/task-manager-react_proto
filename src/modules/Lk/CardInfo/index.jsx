import React from 'react'
import { Form as FinalForm, Field } from 'react-final-form'

import { Divider } from 'ui/components'
import { validation } from 'helpers/formValidation'

import { Title, Description } from './components/Fields'
import UsersList from './components/UsersList'
import ConnectState from './ConnectState'
import { getFormSchema } from './formSchema'

const CardInfo = ({ isCreated, cardId, info, users, submitForm }) => {
	const initialValues = {
		cardId: cardId,
		project_id: 1,
		department_id: 1,
		title: info.title,
		description: info.description,
		date: info.date
	}

	return (
		<FinalForm
			onSubmit={ submitForm }
			validate={ validation(getFormSchema) }
			initialValues={ initialValues }
		>
			{ ({ handleSubmit, submitting, ...restParams }) => (
				<form onSubmit={ handleSubmit } noValidate>
					<Field
						type="text"
						name="title"
						handleSubmit={ handleSubmit }
						placeholder="Введите название задачи"
						maxLength="150"
						component={ Title }
					/>

					{ !!info.project && (
						<span style={ { padding: '5px 10px' } }>Расположение: { `${ info.project.name }, ${ info.project.department.name }` }</span>
					) }

					<Divider style={ { margin: '10px 0' } } />

					<Field
						type="text"
						name="description"
						handleSubmit={ handleSubmit }
						placeholder="Описание задачи"
						component={ Description }
					/>

					<Divider style={ { margin: '10px 0' } } />

					<UsersList title="Участники:" users={ users } />
				</form>
			) }
		</FinalForm>
	)
}

export default ConnectState(CardInfo)
