import { replace } from 'react-router-redux'
import { axios, paths } from 'helpers/api'

export const createdTask = ({ cardId, ...values }) => async(dispatch) => {
	try {
		const { data } = await axios.post(paths.tasks, values)
		dispatch(replace({ pathname: `/c/${ data.id }`, state: { modal: true } }))
	} catch (err) {
		console.log(err)
	}
}
