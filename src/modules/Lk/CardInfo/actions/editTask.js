import { axios, paths } from 'helpers/api'

export const editTask = ({ cardId, ...values }, formApi) => async(dispatch) => {
	// console.log(formApi)
	try {
		await axios.post(`${ paths.tasks }/${ cardId }`, values)
	} catch (err) {
		global.console.log(err)
	}
}
