import React from 'react'

import styles from './ActionsBar.scss'

const ActionsBar = ({ children }) => (
	<div className={ styles['actions-bar_container'] }>
		{ children }
	</div>
)

export default ActionsBar
