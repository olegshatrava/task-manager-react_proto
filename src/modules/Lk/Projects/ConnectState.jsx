/* eslint arrow-body-style: 0 */
import React, { Component } from 'react'
import { axios } from 'helpers/api'

const ConnectStateDepartament = (ChildComponent) => {
	return class DepartamentWrapper extends Component {
		_isMounted = false

		state = {
			lists: [],
			users: []
		}

		componentDidMount() {
			this._isMounted = true
			const { departmentId } = this.props.match.params

			this.getDepartamentList(departmentId)
			this.getDepartamentUsers(departmentId)
		}

		componentWillUnmount() {
			this._isMounted = false
		}

		getDepartamentList = async(departmentId) => {
			try {
				const { data } = await axios.get(`/departments/${ departmentId }/processes`)
				this.setState({ lists: data.data })
			} catch (err) {
				console.log(err)
			}
		}

		getDepartamentUsers = async(departmentId) => {
			try {
				const { data } = await axios.get(`/departments/${ departmentId }/users`)
				this.setState({ users: data.data })
			} catch (err) {
				console.log(err)
			}
		}

		render() {
			const { users, lists } = this.state

			return (
				<ChildComponent
					users={ users }
					lists={ lists }
				/>
			)
		}
	}
}

export default ConnectStateDepartament
