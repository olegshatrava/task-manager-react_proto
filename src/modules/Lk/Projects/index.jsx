import React, { Fragment } from 'react'
import { compose } from 'recompose'
import { Link } from 'react-router-dom'

import { Avatar, Button } from 'ui/components'

import ConnectState from './ConnectState'
import ActionsBar from './components/ActionsBar'

import styles from './Board.scss'

const ProjectsPage = ({ users, lists, ...restProps }) => {
	console.log()

	return (
		<Fragment>
			<ActionsBar>
				{ users.map((item, idx) => {
					const fullUserName = [item.first_name, item.last_name].join(' ')

					return (
						<Avatar
							key={ item.id }
							size="sm"
							src={ item.image ? item.image.thumb : '//placehold.it/150x150' }
							alt={ fullUserName }
							title={ fullUserName }
						/>
					)
				}) }

				<Button size="sm" theme="default" icon={ ['far', 'plus'] } disabled />
			</ActionsBar>

			<div className={ styles['board-container'] }>

				{ lists.map((listItem, idx) => (
					<div key={ listItem.id } className={ styles['list-wrapper'] }>
						<div className={ styles['list-content'] }>
							{/* HEADERS */}
							<div className={ styles['list-header'] }>
								<span>{ listItem.name }</span>
							</div>

							{/* CARDS */}
							<div className={ styles['list-cards'] }>

								{ listItem.tasks.map((taskItem, idx) => (
									<Link
										key={ taskItem.id }
										to={ {
											pathname: `/c/${ taskItem.id }`,
											state: {
												modal: true,
												redirectTo: `${ location.pathname }${ location.search }`
											}
										} }
										className={ styles['list-card'] }
									>
										<p>{ taskItem.title }</p>
									</Link>
								)) }

							</div>

							{/* ADD NEW */}
							{ listItem.type === 1 && (
								<Link to={ { pathname: '/c/new', state: { modal: true, redirectTo: `${ location.pathname }${ location.search }` } } } className={ styles['list-new-task'] }>
									Добавить задачу
								</Link>
							) }
						</div>
					</div>
				)) }

			</div>
		</Fragment>
	)
}

export default compose(
	ConnectState
)(ProjectsPage)
