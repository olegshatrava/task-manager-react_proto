// @flow
import React, { Component } from 'react'
import { hot } from 'react-hot-loader'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import { withRouter, Switch, Route } from 'react-router-dom'

import { hasLoggedInUser } from 'modules/Authentication/selectors'
import { getUserInfo } from 'modules/Lk/Profile/actions/getUserInfo'

import Home from './Home'
import Authentication from './Authentication'
import Logout from './Authentication/modules/Logout'
import Lk from './Lk'

type AppProps = {
	isLoggedIn: boolean,
	dispatch: Function
}

class AppComponent extends Component<AppProps> {
	componentDidMount() {
		const { isLoggedIn, dispatch } = this.props

		if (isLoggedIn) {
			dispatch(getUserInfo())
		}
	}

	render() {
		const { isLoggedIn } = this.props

		return (
			<Switch>
				<Route exact path="/" component={ Home } />
				<Route path="/logout" component={ Logout } />
				<Route component={ isLoggedIn ? Lk : Authentication } />
			</Switch>
		)
	}
}

const mapStateToProps = (state) => ({
	isLoggedIn: hasLoggedInUser(state)
})

export default compose(
	hot(module),
	withRouter,
	connect(mapStateToProps)
)(AppComponent)
