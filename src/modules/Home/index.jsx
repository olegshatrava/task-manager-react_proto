import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

import { Heading, Button } from 'ui/components'

import { Logo } from './components/Logo'

import styles from './Home.scss'

class HomePage extends Component {
	render() {
		if (this.props.isLoggedIn) {
			return <Redirect to="/tasks" />
		}

		return (
			<div className={ styles['container'] }>
				<Logo />
				<Heading type="h3">Система управления проектами</Heading>

				<br/>

				<Button to="/login">Войти</Button>
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	isLoggedIn: state.authentication.isLoggedIn
})

export default connect(mapStateToProps)(HomePage)
