import React from 'react'

import { Heading } from 'ui/components'
import { Logo } from 'modules/Home/components/Logo'

import styles from './Preview.scss'

const Preview = () => (
	<div className={ styles['container'] }>
		<Logo />
		<Heading type="h3">Система управления проектами</Heading>
	</div>
)

export default Preview
