// @flow

/**
* Has check isLoggedIn user.
* @param {Object} state
* @returns {boolean}
*/
export const hasLoggedInUser = (state: Object): boolean => state.authentication.isLoggedIn
