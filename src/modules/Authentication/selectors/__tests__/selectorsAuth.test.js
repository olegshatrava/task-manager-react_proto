import { hasLoggedInUser } from '../index'
import { initialState } from '../../reducers/authentication'

describe('Auth -> selectors', () => {
	let state = initialState

	beforeEach(() => {
		state = {
			authentication: {
				...initialState
			}
		}
	})

	it('get isLoggedIn param.', () => {
		expect(hasLoggedInUser(state)).toEqual(false)
	})
})
