import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { Row, Col } from 'ui/components'

import Preview from './components/Preview'

import Login from './modules/Login'
import Recovery from './modules/Recovery'

const AuthenticationModule = ({ match }) => (
	<Row style={ { minHeight: '100vh' } }>
		<Col md={ 6 }>
			<Preview />
		</Col>

		<Col md={ 6 } style={ { alignSelf: 'center' } }>

			<Switch>
				<Route path="/login" component={ Login } />
				<Route path="/recovery" component={ Recovery } />
				<Redirect to="/login" />
			</Switch>

		</Col>
	</Row>
)

export default AuthenticationModule
