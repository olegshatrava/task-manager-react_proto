import { authentication as reducer } from '../authentication'
import { LOGIN_SUCCESS } from '../../modules/Login/constants'
import { LOGOUT_SUCCESS } from '../../modules/Logout/constants'

describe('Authentication/Login/reducers/isLoggedIn)', () => {
	const initialState = {
		isLoggedIn: false
	}

	it('should return the initial state.', () => {
		expect(reducer(undefined, {})).toEqual(initialState)
	})

	it('should return success login.', () => {
		const successAction = {
			type: LOGIN_SUCCESS
		}

		expect(reducer(initialState, successAction)).toEqual({ isLoggedIn: true })
	})

	it('should return logout user.', () => {
		const successAction = {
			type: LOGOUT_SUCCESS
		}

		expect(reducer(initialState, successAction)).toEqual({ isLoggedIn: false })
	})
})
