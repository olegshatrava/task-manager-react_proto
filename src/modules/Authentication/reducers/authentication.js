import { LOGIN_SUCCESS } from '../modules/Login/constants'
import { LOGOUT_SUCCESS } from '../modules/Logout/constants'
import { getAuthToken } from '../helpers/authToken'

const hasToken = getAuthToken()

export const initialState = {
	isLoggedIn: Boolean(hasToken || false)
}

export const authentication = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_SUCCESS: {
			return {
				isLoggedIn: true
			}
		}

        case LOGOUT_SUCCESS: {
			return {
				isLoggedIn: false
			}
		}

        default:
            return state
    }
}
