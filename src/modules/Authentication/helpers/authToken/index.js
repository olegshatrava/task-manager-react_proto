// @flow
import jsCookie from 'js-cookie'

const AUTH_TOKEN: string = 'token'

/**
* Save Auth token in cookies
* @param {string} token
*/
export const setAuthToken = (token: string): void => jsCookie.set(AUTH_TOKEN, token, { expires: 13 })

/**
* Get Auth token from cookies
*/
export const getAuthToken = (): void => jsCookie.get(AUTH_TOKEN)

/**
* Remove Auth token from cookies
*/
export const removeAuthToken = (): void => jsCookie.remove(AUTH_TOKEN)
