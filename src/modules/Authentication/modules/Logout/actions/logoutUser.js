// @flow
import { LOGOUT_SUCCESS } from '../constants'
import { removeAuthToken } from '../../../helpers/authToken'

export const logoutUser = () => ({
	type: LOGOUT_SUCCESS,
	removeToken: removeAuthToken()
})
