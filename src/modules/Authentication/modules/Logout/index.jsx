import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'

import { logoutUser } from './actions/logoutUser'
import { hasLoggedInUser } from '../../selectors'

export class Logout extends Component {
	componentDidMount() {
		const { isLoggedIn, dispatch } = this.props

		if (isLoggedIn) {
			dispatch(logoutUser())
		}
	}

	render() {
		return <Redirect replace to="/" />
	}
}

const mapStateToProps = (state) => ({
	isLoggedIn: hasLoggedInUser(state)
})

export default connect(mapStateToProps)(Logout)
