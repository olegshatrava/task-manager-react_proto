// @flow
import React from 'react'
import { Form as FinalForm, Field } from 'react-final-form'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Form } from 'antd'
import { Alert, Input, Button, Heading, Form as NewForm } from 'ui/components'
import { validation } from 'helpers/formValidation'

import { loginRequest } from './actions/loginSuccess'
import { getFormSchema } from './formSchema'

export const LoginPage = ({ login, match }: Object) => (
	<div style={ { width: '460px', margin: 'auto' } }>
		<Heading type="h3">Добро пожаловать!</Heading>

		<p>Войдите введя информацию ниже.</p>

		<FinalForm
			onSubmit={ login }
			validate={ validation(getFormSchema) }
			render={ ({ pristine, invalid, submitting, handleSubmit, submitErrors, submitFailed, ...rest }) => (
				<NewForm onSubmit={ handleSubmit } noValidate style={ { marginTop: '50px' } }>
					<Form.Item>
						<Field
							type="email"
							name="email"
							size="lg"
							placeholder="Электронная почта"
							autoComplete="off"
							autoCorrect="off"
							autoCapitalize="none"
							required
							component={ Input }
						/>
					</Form.Item>

					<Form.Item>
						<Field
							type="password"
							name="password"
							size="lg"
							placeholder="Пароль"
							autoComplete="off"
							autoCorrect="off"
							autoCapitalize="none"
							required
							component={ Input }
						/>
					</Form.Item>

					{ !!submitFailed && typeof submitErrors !== 'undefined' && (
						<Form.Item>
							<Alert
								type="error"
								title="Произошла ошибка"
								description={ submitErrors.common || 'Произошла ошибка, попробуйте позже.' }
								showIcon
							/>
						</Form.Item>
					) }

					<Form.Item style={ { textAlign: 'right' } }>
						<Link to="/recovery" style={ { margin: '0 0 10px' } }>Забыли пароль?</Link>

						<Button type="submit" theme="purple" size="lg" block disabled={ submitting }>Войти</Button>
					</Form.Item>

					<Form.Item style={ { textAlign: 'center' } }>
						<span>У вас еще нет аккаунта? <Link to="/">Оставить заявку</Link>.</span>
					</Form.Item>
				</NewForm>
			) }
		/>
	</div>
)

const mapDispatchToProps = (dispatch) => ({
	login: (data, formApi, setErrors) => dispatch(loginRequest(data, formApi, setErrors))
})

// $FlowIgnore
export default connect(null, mapDispatchToProps)(LoginPage)
