import { object, string } from 'yup'

export function getFormSchema(value) {
	return object().shape({
		email: string()
			.email()
			.required(),

		password: string()
			.min(9)
			.required()
	})
}
