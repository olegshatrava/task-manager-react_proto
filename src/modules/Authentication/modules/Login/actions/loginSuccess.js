// @flow
import { axios, paths } from 'helpers/api'
import { replace } from 'react-router-redux'
import { parse } from 'query-string'

import { getUserInfo } from 'modules/Lk/Profile/actions/getUserInfo'
import { setAuthToken } from '../../../helpers/authToken'

import * as types from '../constants'

type UserDataLogin = {|
	email: string,
	password: string
|}

/**
* HTTP Request to login user
* @param {Object} userData - User data for authentication.
* @param {string} userData.email - The email of the user.
* @param {string} userData.password - The password of the user.
* @param {Object} formApi - FormApi from submit login form
* @param {Fucntion} setErrors - Send errors api to the form.
*/
export const loginRequest = ({ email, password }: UserDataLogin, formApi: Object, setErrors: (errors: Object) => void) => async(dispatch: Function): Promise<void> => {
	try {
		const { data } = await axios.post(paths.login, { email, password })
		await setAuthToken(data.data.token)
		await dispatch(getUserInfo())
		await dispatch(loginSuccess())
		await dispatch(redirectToSuccessLogin())
	} catch (err) {
		await setErrors({ common: err.response.data.errors.message })
	}
}

/**
* Actions for login user successfully
*/
export const loginSuccess = () => ({
	type: types.LOGIN_SUCCESS
})

/**
* Redirect if succesfull auth
*/
const redirectToSuccessLogin = () => (dispatch, getState) => {
	const locationSearch: ?string = getState().routing.location.search

	let nextPath: string = '/tasks'

	if (locationSearch && parse(locationSearch) && parse(locationSearch)['to']) {
		nextPath = parse(locationSearch)['to']
	}

	dispatch(replace(nextPath))
}
