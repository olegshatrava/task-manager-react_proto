import { LOGIN_SUCCESS } from '../../constants'
import { loginSuccess } from '../loginSuccess'
// import { mockStore } from 'store/mockStore'

// const mockServiceCreator = (body, succeeds = true) => () => new Promise((resolve, reject) => {
// 	setTimeout(() => (succeeds ? resolve(body) : reject(body)), 10)
// })

describe('Login/actions', () => {
	// let store

	// beforeEach(() => {
	// 	store = mockStore({ isAuth: false })
	// })

	// it('when can request with email and password to api', async() => {
	// 	await store.dispatch(
	// 		loginRequest({ email: 'user', password: 'pass' },
	// 		mockServiceCreator({ data: { result: 'success' } })
	// 	))

	// 	await expect(store.getActions()).toContainEqual(loginSuccess())
	// })

	it('when a user successfully logged in', () => {
		const expectedAction = {
			type: LOGIN_SUCCESS
		}

		expect(loginSuccess()).toEqual(expectedAction)
	})
})
