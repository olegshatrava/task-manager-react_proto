import { object, string } from 'yup'

export function getFormSchema(value) {
	return object().shape({
		email: string().email().required()
	})
}
