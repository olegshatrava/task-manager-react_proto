import React, { Component, Fragment } from 'react'
import { Form as FinalForm, Field } from 'react-final-form'
import { axios, paths } from 'helpers/api'
import { Form } from 'antd'
import { Input, Button, Heading } from 'ui/components'
import { validation } from 'helpers/formValidation'

import { getFormSchema } from './formSchema'

export default class RecoveryPasswordPage extends Component {
	state = {
		isSuccessfull: false,
		currentEmail: null
	}

	handleSubmit = async({ email }, formApi, setError) => {
		try {
			await axios.post(paths.recoveryPasswordEmail, { email })
			await this.setState({ isSuccessfull: true, currentEmail: email })
		} catch (err) {
			setError({ ...err.response.data.errors })
		}
	}

	render() {
		const { isSuccessfull, currentEmail } = this.state

		return (
			<div style={ { width: '450px', margin: 'auto' } }>
				<Heading type="h2">Восстановление пароля</Heading>

				{ isSuccessfull ? (
					<Fragment>
						<p>На электронный адрес <b>{ currentEmail }</b> отправленно письмо.</p>
						<p>Перейдите по ссылке в нем и укажите новый пароль для входа в личный кабинет.</p>
					</Fragment>
				) : (
					<FinalForm
						onSubmit={ this.handleSubmit }
						validate={ validation(getFormSchema) }
						render={ ({ pristine, invalid, submitting, handleSubmit, submitErrors, submitFailed, ...rest }) => (
							<Form onSubmit={ handleSubmit } noValidate>
								<Form.Item>
									<Field
										type="email"
										name="email"
										size="lg"
										placeholder="Электронная почта"
										autoComplete="off"
										autoCorrect="off"
										autoCapitalize="none"
										required
										component={ Input }
									/>
								</Form.Item>

								<Form.Item>
									<Button type="submit" theme="purple" size="lg" block disabled={ submitting }>
										Продолжить
									</Button>
								</Form.Item>
							</Form>
						) }
					/>
				) }
			</div>
		)
	}
}
