import { object, string } from 'yup'

export function getFormSchema(value) {
	return object().shape({
		password: string()
			.min(9)
			.required(),

		passwordConfirmation: string()
			.min(9)
			.required()
			.test('password', 'Пароли не совпадают', function(value) {
				return value === this.parent.password
			})
	})
}
