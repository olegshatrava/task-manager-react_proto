import React, { Component } from 'react'
import qs from 'query-string'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import { replace } from 'react-router-redux'
import { withRouter } from 'react-router-dom'
import { Form as FinalForm, Field } from 'react-final-form'
import { Form } from 'antd'
import { Input, Button, Heading } from 'ui/components'

import { axios, paths } from 'helpers/api'
import { validation } from 'helpers/formValidation'

import { getFormSchema } from './formSchema'

class ConfirmationRecoveryPassword extends Component {
	handleSubmit = async({ password, passwordConfirmation }, formApi, setErrors) => {
		const { match, location, dispatch } = this.props
		const { email } = qs.parse(location.search)
		const { token } = match.params

		try {
			await axios.post(paths.recoveryPassword, {
				token,
				email,
				password,
				password_confirmation: passwordConfirmation
			})
			await dispatch(replace('/login'))
		} catch (err) {
			setErrors({ ...err.response.data.errors })
		}
	}

	render() {
		return (
			<div style={ { width: '450px', margin: 'auto' } }>

				<Heading type="h2">Укажите новый пароль</Heading>

				<FinalForm
					onSubmit={ this.handleSubmit }
					validate={ validation(getFormSchema) }
					render={ ({ pristine, invalid, submitting, handleSubmit, submitErrors, submitFailed, ...rest }) => (
						<Form onSubmit={ handleSubmit } noValidate>
							<Form.Item>
								<Field
									type="password"
									name="password"
									placeholder="Новый пароль"
									size="lg"
									autoComplete="off"
									autoCorrect="off"
									autoCapitalize="none"
									required
									component={ Input }
								/>
							</Form.Item>

							<Form.Item>
								<Field
									type="password"
									name="passwordConfirmation"
									placeholder="Повтор пароля"
									size="lg"
									autoComplete="off"
									autoCorrect="off"
									autoCapitalize="none"
									required
									component={ Input }
								/>
							</Form.Item>

							<Form.Item>
								<Button type="submit" theme="purple" size="lg" block disabled={ submitting }>
									Сохранить
								</Button>
							</Form.Item>
						</Form>
					) }
				/>
			</div>
		)
	}
}

export default compose(
	withRouter,
	connect()
)(ConfirmationRecoveryPassword)
