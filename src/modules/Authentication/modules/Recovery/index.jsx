import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'

import RequestRecoveryPassword from './modules/Request'
import ConfirmationPassword from './modules/Confirmation'

const RecoveryPage = () => (
	<Switch>
		<Route exact path="/recovery" component={ RequestRecoveryPassword } />
		<Route path="/recovery/confirm/:token" component={ ConfirmationPassword } />
		<Redirect to="/recovery" />
	</Switch>
)

export default RecoveryPage
