import { createStore, applyMiddleware, compose } from 'redux'
import { routerMiddleware } from 'react-router-redux'
import thunkMiddleware from 'redux-thunk'
import promiseMiddleware from 'redux-promise-middleware'

import mainApi from 'helpers/api/axios'

import rootReducer from './rootReducer'

const enableReduxDevTools = process.env.REACT_REDUX_DEVTOOLS === 'true'

export const configureStore = (initialState = {}, history) => {
    const middlewares = [
        routerMiddleware(history),
        promiseMiddleware(),
        thunkMiddleware.withExtraArgument({ mainApi })
    ]

    const enhancers = [
        applyMiddleware(...middlewares)
	]

	let composeEnhancers = compose

	if (enableReduxDevTools && typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
		composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({ shouldHotReload: false })
	}

    const store = createStore(
        rootReducer,
        initialState,
        composeEnhancers(...enhancers)
    )

	if (module.hot) {
        module.hot.accept('./rootReducer', () => {
			const nextRootReducer = require('./rootReducer').default
            store.replaceReducer(nextRootReducer)
        })
    }

    return store
}
