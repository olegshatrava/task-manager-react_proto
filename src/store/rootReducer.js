import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'

import { authentication } from 'modules/Authentication/reducers/authentication'
import { userInfoCombine } from 'modules/Lk/Profile/reducers'
import { sidebar } from 'modules/Lk/shared/Sidebar/reducers'

export default combineReducers({
	routing: routerReducer,
	authentication,
	userInfo: userInfoCombine,
	sidebar
})
