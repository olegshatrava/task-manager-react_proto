// @flow
import React, { Fragment, type Node } from 'react'
import classnames from 'classnames/bind'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import styles from './styles/Button.scss'
const cn = classnames.bind(styles)

export type ButtonTypes = 'button' | 'submit' | 'reset'
export type ButtonThemes = 'default' | 'purple' | 'success' | 'danger'
export type ButtonShape = 'circle' | 'square'
export type ButtonSize = 'sm' | 'md' | 'lg'

export type ButtonProps = {|
	type?: ButtonTypes,
	theme?: ButtonThemes,
	shape?: ButtonShape,
	size?: ButtonSize,
	to?: string | Object,
	linkProps?: Object,
	icon?: string | ['fal' | 'far' | 'fas', string],
	tabIndex?: number,
	block?: boolean,
	transparent?: boolean,
	disabled?: boolean,
	className?: string,
	classNameIcon?: string,
	children?: Object | string,
	onClick?: () => void
|}

const Button = ({
	type = 'button',
	theme = 'default',
	size = 'md',
	shape,
	to,
	linkProps,
	icon,
	block,
	transparent,
	className,
	classNameIcon,
	children,
	...otherProps
}: ButtonProps): Node => {
	const classesWithParams: string = cn(
		!!theme && `theme-${ theme }`,
		!!size && `size-${ size }`,
		!!shape && `shape-${ shape }`,
		(!!icon && !children) && `is-icon`,
		!!block && 'block',
		!!transparent && 'transparent'
	)

	const childrenComponent: Node = (
		<Fragment>
			{ !!icon && <FontAwesomeIcon icon={ icon } className={ cn(classNameIcon) } /> }
			{ !!children && <span className={ cn(!!icon && 'child-with-icon') }>{ children }</span> }
		</Fragment>
	)

	if (typeof to !== 'undefined') {
		return (
			<Link to={ to } className={ cn('btn', classesWithParams, className) } { ...linkProps }>
				{ childrenComponent }
			</Link>
		)
	}

	return (
		<button
			type={ type }
			className={ cn('btn', classesWithParams, className) }
			{ ...otherProps }
		>
			{ childrenComponent }
		</button>
	)
}

export default Button
