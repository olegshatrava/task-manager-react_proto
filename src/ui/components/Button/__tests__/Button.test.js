import React from 'react'
import { shallow } from 'enzyme'

import Button from '../Button'

describe('UI Button', () => {
	it('should render without problem', () => {
		const wrapper = shallow(<Button type="button">Button</Button>)
		expect(wrapper).toMatchSnapshot()
	})

	it('should render with type submit', () => {
		const wrapper = shallow(<Button type="submit">Submit</Button>)
		expect(wrapper.prop('type')).toEqual('submit')
	})
})
