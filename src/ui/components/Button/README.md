# Size

* props {string} - `size`
* default: `md`
* variations: `'sm'` | `'md'` | `'lg'`

```jsx
<Button size="sm">Small</Button>
<Button size="md">Medium (Default)</Button>
<Button size="lg">Large</Button>
```

# Themes

* props {string} - `theme`
* default: `'default'`
* variations: `'default'` | `'purple'`

```jsx
<Button theme="default">Default (default)</Button>
<Button theme="purple">Purple</Button>
```

# Block

* props {boolean} - `block`
* default: `false`
* variations: `true` | `false`

```jsx
<Button block>Block botton (width: 100%)</Button>
```