// @flow
import React, { Component } from 'react'
import classnames from 'classnames/bind'

import styles from './styles/Form.scss'
const cn = classnames.bind(styles)

export type FormProps = {
	className?: string,
	noValidate?: boolean,
	autocomplete?: boolean,
	id?: string,
	name?: string,
	children: any,
	onSubmit: (evt: Event) => void,
}

class Form extends Component<FormProps> {
	static defaultProps = {
		noValidate: false,
		autocomplete: true
	}

	submitForm = (evt: Event): void => {
		evt.preventDefault()

        if (this.props.onSubmit) {
            this.props.onSubmit(evt)
        }
	}

	render() {
		const { className, noValidate, autocomplete, id, name, ...rest } = this.props

		return (
			<form
				noValidate={ noValidate }
				autoComplete={ autocomplete === false ? 'off' : 'on' }
				className={ cn(className) }
				id={ id }
                name={ name }
				onSubmit={ this.submitForm }
				{ ...rest }
			>
				{ this.props.children }
			</form>
		)
	}
}

export default Form
