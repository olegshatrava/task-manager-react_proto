import React from 'react'
import { shallow } from 'enzyme'

import Input from '../Input'

describe('UI Input', () => {
	let wrapper

	let meta = {
		active: false,
		touched: false,
		error: null
	}

	let input = {
		value: 'test input',
		onChange: jest.fn(),
		onBlur: jest.fn(),
		onFocus: jest.fn()
	}

	beforeEach(() => {
		wrapper = shallow(
			<Input
				type="text"
				placeholder="Test email"
				meta={ meta }
				input={ input }
			/>
		)
	})

	it('should render without problem.', () => {
		expect(wrapper).toMatchSnapshot()
	})
})
