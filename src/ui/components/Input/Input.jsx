// @flow
import React, { Component } from 'react'
import classnames from 'classnames/bind'

import Caption from 'ui/components/Typography/Caption'

import styles from './styles/Input.scss'
const cx = classnames.bind(styles)

const stylePrefix = 'ui-input'

export type InputProps = {|
	type: 'text' | 'number' | 'email' | 'password' | 'tel',
	size: 'sm' | 'md' | 'lg',
	label: string,
	placeholder?: string,
	readOnly?: boolean,
	disabled?: boolean,
	input: {
		name: string,
		value: string,
		onFocus: (evt: SyntheticInputEvent<*>) => void,
		onBlur: (evt: SyntheticInputEvent<*>) => void,
		onChange: (evt: SyntheticInputEvent<*>) => void
	},
	meta: {
		touched: boolean,
		error: null | string,
		submitError: null | string,
		active: boolean
	}
|}

export default class Input extends Component<InputProps> {
	static defaultProps = {
		size: 'md'
	}

	render() {
		const { type, label, size, placeholder, readOnly, disabled, input, meta, ...otherParams } = this.props

		const isError: boolean = !!meta.touched && (!!meta.error || !!meta.submitError)

		return (
			<div className={ cx(stylePrefix, !!isError && `${ stylePrefix }__error`) }>
				<input
					type={ type }
					value={ input.value }
					className={ cx(`${ stylePrefix }_field`, `${ stylePrefix }_field-size--${ size }`) }
					placeholder={ placeholder }
					onFocus={ input.onFocus }
					onBlur={ input.onBlur }
					onChange={ input.onChange }
					readOnly={ readOnly }
					disabled={ disabled }
					{ ...otherParams }
				/>

				{ isError && <Caption className={ styles.uiInputError }>{ meta.error || meta.submitError }</Caption> }
			</div>
		)
	}
}
