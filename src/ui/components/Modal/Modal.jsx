import React, { Component } from 'react'
import RcDialog from 'rc-dialog'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import classnames from 'classnames/bind'

import './styles/RcDialog.scss'
import styles from './styles/Modal.scss'
const cn = classnames.bind(styles)

class Modal extends Component {
	static defaultProps = {
		visible: false,
		center: false,
		destroyOnClose: true
	}

	state = {
		mousePosition: {}
	}

	render() {
		const { mousePosition } = this.state
		const { visible, center, destroyOnClose, onClose, ...restProps } = this.props

		const classes = cn({
			'center': !!center
		})

		return (
			<RcDialog
				prefixCls="UiDialog"
				visible={ visible }
				wrapClassName={ classes }
				onClose={ onClose }
				mousePosition={ mousePosition }
				destroyOnClose={ destroyOnClose }
				closeIcon={ <FontAwesomeIcon icon={ ['fal', 'times'] } /> }
				{ ...restProps }
			>
				{ this.props.children }
			</RcDialog>
		)
	}
}

export default Modal
