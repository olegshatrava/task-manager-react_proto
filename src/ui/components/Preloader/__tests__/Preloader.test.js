import React from 'react'
import { shallow } from 'enzyme'

import Preloader from '../Preloader'

describe('UI Preloader', () => {
	it('should render without problem', () => {
		const wrapper = shallow(
			<Preloader />
		)

		expect(wrapper).toMatchSnapshot()
	})
})
