# Preloader

## Property
| Prop | Description | Type | Default | Required
|---|---|---|---|---|---|
| size | Размеры `sm`, `md`, `lg` | *{string}*  | `md` | - |
| className | Передать внешние классы | *{string}* | - | - |

## Use
```jsx
import { Preloader } from 'ui/components'
or
import Preloader from 'ui/components/Preloader'

<Preloader />
```