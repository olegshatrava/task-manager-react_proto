// @flow
import React from 'react'
import classnames from 'classnames/bind'

import styles from './styles/Preloader.scss'
const cn = classnames.bind(styles)

type PreloaderProps = {|
	size?: 'sm' | 'md' | 'lg',
	className?: string
|}

const Preloader = ({
	size = 'md',
	className
}: PreloaderProps) => {
	const classes: string = cn('wrapper', [`size__${ size }`], className)

	return (
		<div className={ classes }>
			<svg width="200px" height="200px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
				<rect x="19" y="21.519" width="12" height="56.9621" fill="#93dbe9">
					<animate attributeName="y" calcMode="spline" values="16;30;30" keyTimes="0;0.5;1" dur="1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1" begin="-0.2s" repeatCount="indefinite"></animate>

					<animate attributeName="height" calcMode="spline" values="68;40;40" keyTimes="0;0.5;1" dur="1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1" begin="-0.2s" repeatCount="indefinite"></animate>
				</rect>

				<rect x="44" y="30" width="12" height="40" fill="#689cc5">
					<animate attributeName="y" calcMode="spline" values="19.5;30;30" keyTimes="0;0.5;1" dur="1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1" begin="-0.1s" repeatCount="indefinite"></animate>

					<animate attributeName="height" calcMode="spline" values="61;40;40" keyTimes="0;0.5;1" dur="1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1" begin="-0.1s" repeatCount="indefinite"></animate>
				</rect>

				<rect x="69" y="30" width="12" height="40" fill="#5e6fa3">
					<animate attributeName="y" calcMode="spline" values="23;30;30" keyTimes="0;0.5;1" dur="1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1" begin="0s" repeatCount="indefinite"></animate>

					<animate attributeName="height" calcMode="spline" values="54;40;40" keyTimes="0;0.5;1" dur="1" keySplines="0 0.5 0.5 1;0 0.5 0.5 1" begin="0s" repeatCount="indefinite"></animate>
				</rect>
			</svg>
		</div>
	)
}

export default Preloader
