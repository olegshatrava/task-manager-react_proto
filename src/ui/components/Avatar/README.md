# Avatar

## Property
| Prop | Description | Type | Default | Required
|---|---|---|---|---|---|
| src | Адрес изображения | *{string}* | `https://placeholder.com/350x350` | - |
| alt | Описание изображения | *{string}* | `Avatar` | - |
| size | Размеры `sm`, `md`, `lg`, либо размеры `100` | *{string/number}*  | `md` | - |
| shape | Форма `circle`, `square`  | *{string}*  | `cirle` | - |
| title | Тайтл | *{string}*  | - | - |
| className | Внешние классы | *{string}* | - | - |

## Use
```jsx
import { Avatar } from 'ui/components'
or
import Avatar from 'ui/components/Avatar'

<Avatar src="https://placeholder.com/350x350" alt="Alt for img" />
```