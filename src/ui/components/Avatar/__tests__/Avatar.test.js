import React from 'react'
import { shallow } from 'enzyme'

import Avatar from '../Avatar'

describe('UI Avatar', () => {
	const initialProps = {
		src: 'https://pp.userapi.com/c846122/v846122926/cd1af/-FKVxvK3vGc.jpg'
	}

	it('should render without problem', () => {
		const wrapper = shallow(
			<Avatar { ...initialProps } />
		)

		expect(wrapper.find('img').filterWhere((item) => item.prop('src') === initialProps.src)).toHaveLength(1)
		expect(wrapper).toMatchSnapshot()
	})

	it('should render Avatar shape="square"', () => {
		const wrapper = shallow(
			<Avatar shape="square" { ...initialProps } />
		)

		expect(wrapper.find('.container').hasClass('container-square')).toEqual(true)
	})
})
