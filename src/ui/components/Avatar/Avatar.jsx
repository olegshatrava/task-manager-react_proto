// @flow
import React, { Component } from 'react'
import classnames from 'classnames/bind'

import styles from './styles/Avatar.scss'
const cn = classnames.bind(styles)

export type AvatarProps = {
	size?: 'sm' | 'md' | 'lg' | number,
	shape?: 'circle' | 'square',
	src: string,
	alt?: string,
	title?: string,
	className?: string,
	style?: Object
}

class Avatar extends Component <AvatarProps> {
	static defaultProps = {
		size: 'md',
		shape: 'square',
		src: 'https://via.placeholder.com/350x350',
		alt: 'Avatar'
	}

	render() {
		const { size, shape, src, alt, title, className } = this.props

		const sizeClasses = cn({
			[`container-sm`]: size === 'sm',
			[`container-md`]: size === 'md',
			[`container-lg`]: size === 'lg'
		})

		const shapeClasses = cn({
			[`container-circle`]: shape === 'circle',
			[`container-square`]: shape === 'square'
		})

		const classes: string = cn('container', sizeClasses, shapeClasses, className)

		return (
			<span className={ classes }>
				<img
					src={ src }
					alt={ alt }
					title={ title }
				/>
			</span>
		)
	}
}

export default Avatar
