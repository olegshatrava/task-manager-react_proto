# Icons

Using icon libs **font-awesome** https://fontawesome.com/icons

To add icons in list import:
---
Example: site icon to `address-book`, `import { faAddressBook }` and `library.add(faAddressBook)`.

Use in components as `address-book`.

