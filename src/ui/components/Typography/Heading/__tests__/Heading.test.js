import React from 'react'
import { shallow } from 'enzyme'

import Heading from '../Heading'

describe('Heading', () => {
	it('should render without problem', () => {
		const wrapper = shallow(<Heading type="h2">Heading</Heading>)

		expect(wrapper).toMatchSnapshot()
	})
})
