//  @flow
import React, { type Node } from 'react'
import classnames from 'classnames/bind'

import styles from './styles/Heading.scss'
const cn = classnames.bind(styles)

export type HeadingProps = {
	type: 'h1' | 'h2' | 'h3' | 'h4',
	className?: string,
	id?: string,
	children: any
}

const Heading = ({ type = 'h1', className, children, ...restProps }: HeadingProps): Node => {
	const nextProps = {
		className: cn('heading', [`size__${ type }`], className),
		id: restProps.id,
		...restProps
	}

	return React.createElement(type,
		nextProps,
		children
	)
}

export default Heading
