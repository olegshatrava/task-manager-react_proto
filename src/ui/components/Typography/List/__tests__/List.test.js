import React from 'react'
import { shallow } from 'enzyme'

import List from '../List'

describe('List', () => {
	const listItems = [
		{
			key: 'one',
			value: 'One item'
		}, {
			key: 'second',
			value: 'Second item'
		}
	]

	it('should render without problem', () => {
		const wrapper = shallow(<List items={ listItems } />)

		expect(wrapper.find('ul').hasClass('list-default')).toEqual(true)
		expect(wrapper).toMatchSnapshot()
	})

	it('should render List type="ordered"', () => {
		const wrapper = shallow(<List type="ordered" items={ listItems } />)

		expect(wrapper.find('ol').hasClass('list-ordered')).toEqual(true)
	})
})
