```jsx

const listItems = [
	{
		key: 'test-1',
		value: 'One'
	}, {
		key: 'test-2',
		value: 'Second'
	}
]

<div>
    <List items={ listItems }>Обычный список</List>

    <List type="ordered" items={ listItems }>Нумерованный список</List>
</div>
```