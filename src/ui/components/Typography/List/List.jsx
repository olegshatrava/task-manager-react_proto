// @flow
import React, { type Node } from 'react'
import classnames from 'classnames/bind'

import styles from './styles/List.scss'
const cn = classnames.bind(styles)

export type ListTypes = 'default' | 'ordered'
export type ListItems = $ReadOnlyArray<{ key: string, value: string }>
export type ListProps = {
	type: ListTypes,
	items: ListItems,
	id?: string
}

const List = ({ type = 'default', items = [], children, ...props }: ListProps): Node => {
	const listElement = (type === 'ordered') ? 'ol' : 'ul'

	const listProps = {
		className: cn('list', [`list-${ type }`]),
		id: props.id
	}

	const listContent = items.map(item => (
		<li
			key={ `item-${ item.key }` }
			className={ cn('list__item') }
		>
			{ item.value }
		</li>
	))

	return React.createElement(
		listElement,
		listProps,
		listContent
	)
}

export default List
