import React from 'react'
import { shallow } from 'enzyme'

import Caption from '../Caption'

describe('Caption', () => {
	it('should render without problem', () => {
		const wrapper = shallow(<Caption>Caption</Caption>)

		expect(wrapper).toMatchSnapshot()
	})
})
