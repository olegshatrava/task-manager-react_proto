// @flow
import React, { type Node } from 'react'
import classnames from 'classnames/bind'

import styles from './styles/Caption.scss'
const cx = classnames.bind(styles)

type Props = {
	children: any,
	className?: string
}

const Caption = ({ className, children, ...otherProps }: Props): Node => (
	<span className={ cx('caption', className) } { ...otherProps }>{ children }</span>
)

export default Caption
