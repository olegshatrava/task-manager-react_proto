# Paragraph

*Props:*
* type: {stirng} - `default` | `lead`
* className?: {stirng}
* id?: {stirng}

```jsx
<Paragraph>Обычный параграф</Paragraph>

<Paragraph type="lead">Параграф для выделения акцента (описание раздела)</Paragraph>
```