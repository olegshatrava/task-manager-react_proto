// @flow
import React from 'react'
import classnames from 'classnames/bind'

import styles from './styles/Paragraph.scss'
const cn = classnames.bind(styles)

type ParagraphProps = {
	type: 'normal' | 'lead',
	className?: string,
	id?: string,
	children: any
}

const Paragraph = ({ type = 'normal', className, id, children }: ParagraphProps) => (
	<p className={ cn('paragraph', [`paragraph__${ type }`], className) } id={ id }>
		{ children }
	</p>
)

export default Paragraph
