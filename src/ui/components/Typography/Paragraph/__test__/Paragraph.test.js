import React from 'react'
import { shallow } from 'enzyme'

import Paragraph from '../Paragraph'

describe('Paragraph', () => {
	it('should render without problem', () => {
		const wrapper = shallow(<Paragraph>Paragraph normal</Paragraph>)

		expect(wrapper).toMatchSnapshot()
	})

	it('should render "p" with text inside', () => {
		const wrapper = shallow(<Paragraph>Paragraph text inside</Paragraph>)

		expect(wrapper.is('p')).toBe(true)
        expect(wrapper.text()).toContain('Paragraph text inside')
	})

	it('should render Paragraph type="lead"', () => {
		const wrapper = shallow(<Paragraph type="lead">Paragraph lead</Paragraph>)

		expect(wrapper.find('p').hasClass('paragraph__lead')).toEqual(true)
	})
})
