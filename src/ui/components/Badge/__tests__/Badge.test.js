import React from 'react'
import { shallow } from 'enzyme'

import Badge from '../Badge'

describe('UI Badge', () => {
	const initialProps = {
		value: '13',
		bigValue: '110',
		maxValue: '99'
	}

	it('should render without problem', () => {
		const wrapper = shallow(<Badge value={ initialProps.value } theme="warning" size="md" />)
		expect(wrapper.find('span').text()).toEqual(initialProps.value)
		expect(wrapper).toMatchSnapshot()
	})

	it('should render without value', () => {
		const wrapper = shallow(<Badge size="md" />)
		expect(wrapper).toMatchSnapshot()
	})
})
