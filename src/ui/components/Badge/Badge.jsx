// @flow
import React, { type Node } from 'react'
import classnames from 'classnames/bind'

import styles from './styles/Badge.scss'
import { checkLength } from './checkLength'
const cn = classnames.bind(styles)

export type BadgeThemes = 'success' | 'warning' | 'danger'
export type BadgeSize = 'sm' | 'md' | 'lg'

export type BadgeProps = {|
	theme?: BadgeThemes,
	size?: BadgeSize,
	isDoted?: boolean,
	value?: string,
	maxValue?: number,
	className?: string
|}

const Badge = ({
	theme = 'danger',
	size = 'md',
	isDoted = false,
	value,
	maxValue,
	className
}: BadgeProps): Node => {
	const classes: string = cn('container',
		theme && `theme-${ theme }`,
		isDoted ? `size-doted-${ size }` : `size-${ size }`,
		className
	)
	return (
		<div className={ classes }>
			{ !isDoted && !!value && <span>{ checkLength(value, maxValue) }</span> }
		</div>
	)
}

export default Badge
