# Badge

## Property
| Prop | Description | Type | Default | Required
|---|---|---|---|---|---|
| theme | Цветовая схема `success`, `warning`, `danger`  | *{string}*  | `danger` | - |
| size | Размеры `sm`, `md`, `lg` | *{string}*  | `md` | - |
| value | Значение счетчика | *{number}* | - | - |

## Use
```jsx
import { Badge } from 'ui/components'
or
import Badge from 'ui/components/Badge'

<Badge
	value="10"
	maxValue="99"
	theme="warning"
	size="md"
	isDoted="true" />
```
