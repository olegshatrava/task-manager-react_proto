export const checkLength = (value, maxValue) => {
	if (!maxValue) { return value }

	const number = parseInt(value)
	return number < maxValue ? number : `${ maxValue }+`
}
