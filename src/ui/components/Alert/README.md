# Alert

## Property
| Prop | Description | Type | Default | Required
|---|---|---|---|---|---|
| type | Состояние алерта `success`, `warning`, `error`, `info` | *{string}*  | `warning` | + |
| title | Название алерта, краткое описание | *{string}* | - | + |
| desciption | Подробное описание проблемы. Юзается вместе с `title` | *{string}* | - | - |
| className | Передать внешние классы | *{string}* | - | - |
| showIcon | Отображать иконку | *{boolean}* | `false` | - |
| iconType | Передать свою иконку из **font-awesome** | *{string}* | - | - |

## Use
```jsx
import { Alert } from 'ui/components'
or
import Alert from 'ui/components/Alert'

<Alert
    type="error"
    title="Ошибка"
    desciption="Произошла ошибка, попробуйте позже"
    showIcon
/>
```