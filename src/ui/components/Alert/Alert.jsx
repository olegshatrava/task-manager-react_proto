// @flow
import React, { Component, type Node } from 'react'
import classnames from 'classnames/bind'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import styles from './styles/Alert.scss'
const cn = classnames.bind(styles)

export type AlertType = 'success' | 'warning' | 'error' | 'info'
export type AlertProps = {
	type: AlertType,
	title: string | Node,
	description: string | Node,
	showIcon: boolean,
	iconType?: string,
	className?: string,
	style?: Object
}

class Alert extends Component<AlertProps> {
	static defaultProps = {
		type: 'warning',
		showIcon: false
	}

	getIconToType = (type: AlertType, iconType?: string): string => {
		if (!!iconType && typeof iconType === 'string') {
			return iconType
		}

		const listIconToType = {
			'success': 'check-circle',
			'warning': 'exclamation-circle',
			'error': 'times-circle',
			'info': 'info-circle'
		}

		return listIconToType[type]
	}

	render() {
		const { type, title, description, showIcon, iconType, className, ...restProps } = this.props

		const classes = cn(
			'alert',
			[`theme-${ type }`],
			!!description && 'alert-with-description',
			!!showIcon && 'alert-with-icon',
			className
		)

		return (
			<div className={ classes } { ...restProps }>
				{ !!showIcon && <FontAwesomeIcon icon={ this.getIconToType(type, iconType) } className={ styles['alert-icon'] } /> }
				<span className={ styles['alert-title'] }>{ title }</span>
				{ !!description && <span className={ styles['alert-description'] }>{ description }</span> }
			</div>
		)
	}
}

export default Alert
