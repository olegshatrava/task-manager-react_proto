import React from 'react'
import { shallow, mount } from 'enzyme'

import 'ui/components/Icons'
import Alert from '../Alert'

describe('UI Alert', () => {
	it('should render without problem', () => {
		const wrapper = shallow(
			<Alert type="success" title="Успешно" />
		)

		expect(wrapper.find('.alert-title').text()).toContain('Успешно')
		expect(wrapper.find('.alert').hasClass('theme-success')).toEqual(true)
		expect(wrapper).toMatchSnapshot()
	})

	it('render with description', () => {
		const wrapper = shallow(
			<Alert type="error" title="Произошла ошибка" description="Неверный логин или пароль" />
		)

		expect(wrapper.find('.alert').hasClass('alert-with-description')).toEqual(true)
		expect(wrapper.find('.alert-title').text()).toContain('Произошла ошибка')

		// Элемент вывода описания алерта
		const descriptionNode = wrapper.find('.alert-description')
		expect(descriptionNode).toHaveLength(1)
		expect(descriptionNode.text()).toContain('Неверный логин или пароль')
	})

	it('render with icon', () => {
		const wrapper = mount(
			<Alert type="success" title="Успешно" showIcon />
		)

		expect(wrapper.prop('showIcon')).toEqual(true)
		expect(wrapper.find('.alert').hasClass('alert-with-icon')).toEqual(true)
		expect(wrapper.find('FontAwesomeIcon')).toHaveLength(1)
	})
})
