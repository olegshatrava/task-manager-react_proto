import React from 'react'
import { Row } from 'react-flexbox-grid'

const RowCustom = ({ children, ...rest }) => (
	<Row { ...rest }>
		{ children }
	</Row>
)

export default RowCustom
