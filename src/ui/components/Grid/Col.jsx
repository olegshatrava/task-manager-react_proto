import React from 'react'
import classnames from 'classnames/bind'
import { Col } from 'react-flexbox-grid'

import styles from './styles/Grid.scss'
const cn = classnames.bind(styles)

const ColCustom = ({ className, children, ...rest }) => (
	<Col className={ cn(className) } { ...rest }>
		{ children }
	</Col>
)

export default ColCustom
