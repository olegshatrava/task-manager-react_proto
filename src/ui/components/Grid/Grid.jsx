import React from 'react'
import { Grid } from 'react-flexbox-grid'

const GridCustom = ({ children, ...rest }) => (
	<Grid { ...rest }>
		{ children }
	</Grid>
)

export default GridCustom
