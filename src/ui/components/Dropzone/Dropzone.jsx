import React from 'react'
import ReactDropzone from 'react-dropzone'
import classnames from 'classnames/bind'

import styles from './styles/Dropzone.scss'
const cn = classnames.bind(styles)

const Dropzone = ({ className, children, ...restProps }) => {
	const classes = cn('container', className)

	return (
		<ReactDropzone className={ classes } { ...restProps }>
			{ children }
		</ReactDropzone>
	)
}

export default Dropzone
