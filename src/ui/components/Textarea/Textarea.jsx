// @flow strict
import React from 'react'
import classnames from 'classnames/bind'
import TextareaAutosize from 'react-textarea-autosize'

import styles from './styles/Textarea.scss'
const cn = classnames.bind(styles)

export type TextareaProps = {
	name: string,
	value?: string,
	defaultValue?: string,
	size: 'sm' | 'md' | 'lg',
	placeholder?: string,
	className?: string,
	minRows?: number,
	maxRows?: number,
	readOnly?: boolean,
	disabled?: boolean,
	inputRef: () => void,
	onFocus: (evt: SyntheticInputEvent<*>) => void,
	onBlur: (evt: SyntheticInputEvent<*>) => void,
	onChange: (evt: SyntheticInputEvent<*>) => void
}

const Textarea = ({
	name,
	size = 'md',
	inputRef,
	className,
	...restProps
}: TextareaProps) => {
	const classes = cn('field', `field-size--${ size }`, className)

	return (
		<TextareaAutosize
			inputRef={ inputRef }
			name={ name }
			className={ classes }
			{ ...restProps }
		/>
	)
}

export default Textarea
