import React from 'react'
import { shallow } from 'enzyme'

import Textarea from '../Textarea'

describe('UI Textarea', () => {
	it('should render without problem.', () => {
		const wrapper = shallow(
			<Textarea
				name="test"
				placeholder="Введите текст"
				value="test test test"
				minRows={ 1 }
				maxRows={ 5 }
			/>
		)

		expect(wrapper).toMatchSnapshot()
	})
})
