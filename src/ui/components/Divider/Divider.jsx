// @flow
import React, { type Node } from 'react'
import classnames from 'classnames/bind'

import styles from './styles/Divider.scss'
const cn = classnames.bind(styles)

export type DividerProps = {|
	type?: 'vertical' | 'horizontal',
	theme?: 'white | black | light-blue',
	className?: string,
	style: Object
|}

const Divider = ({
	type = 'horizontal',
	theme = 'black',
	className,
	...restProps
}: DividerProps): Node => {
	const classes: string = cn(
		'container',
		!!type && `type-${ type }`,
		!!theme && `theme-${ theme }`,
		className
	)
	return (
		<div className={ classes } { ...restProps } />
	)
}

export default Divider
