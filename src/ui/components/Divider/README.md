# Divider

## Property
| Prop | Description | Type | Default | Required
|---|---|---|---|---|---|
| type | Тип `horizontal`, `vertical`  | *{string}*  | `horizontal` | - |
| theme | Цветовая схема `white`, `black`, `light-blue`  | *{string}*  | `black` | - |
| className | Внешние классы | *{string}* | - | - |

## Use
```jsx
import { Divider } from 'ui/components'
or
import Divider from 'ui/components/Divider'

<Divider type="horizontal" theme="light-blue" className={ styles['divider'] } />
```
