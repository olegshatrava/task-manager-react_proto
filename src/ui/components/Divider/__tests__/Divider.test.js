import React from 'react'
import { shallow } from 'enzyme'

import Divider from '../Divider'

describe('UI Divider', () => {
	it('should render without problem', () => {
		const wrapper = shallow(<Divider type="vertical" theme="light-blue" />)
		expect(wrapper).toMatchSnapshot()
	})

	it('should render with default props', () => {
		const wrapper = shallow(<Divider />)
		expect(wrapper).toMatchSnapshot()
	})
})
