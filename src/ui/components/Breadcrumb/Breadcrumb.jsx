// @flow
import React, { type Node } from 'react'
import { withRouter } from 'react-router-dom'

import BreadcrumbItem from './BreadcrumbItem'

import styles from './styles/Breadcrumb.scss'

export type BreadcrumbProps = {
	routes: Object,
	match: Object,
	location: Object
}

export const Breadcrumb = ({
	routes,
	match,
	location
}: BreadcrumbProps): Node => {
	const pathSnippets: (?string)[] = location.pathname.split('/').filter(i => i)

	const breadcrumbItems: Node[] = pathSnippets.map((pathname, idx) => {
		const url: string = `/${ pathSnippets.slice(0, idx + 1).join('/') }`

		if (!routes[url]) {
			return null
		}

		return <BreadcrumbItem key={ url } to={ url } title={ routes[url] } />
	}).filter(React.isValidElement)

	const countChildren: number = breadcrumbItems.length - 1

	return (
		<ul className={ styles['breadcrumb-list'] }>
			{ React.Children.map(breadcrumbItems, (child: Object, idx: number) => {
				if (child) {
					return React.cloneElement(child, {
						lastItem: countChildren === idx
					})
				}
			}) }
		</ul>
	)
}

export default withRouter(Breadcrumb)
