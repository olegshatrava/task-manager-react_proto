// @flow
import React, { type Node } from 'react'
import classnames from 'classnames/bind'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import styles from './styles/Breadcrumb.scss'
const cn = classnames.bind(styles)

export type BreadcrumbItemProps = {|
	to: string,
	title: string,
	lastItem?: boolean
|}

const BreadcrumbItem = ({ to, title, lastItem }: BreadcrumbItemProps): Node => {
	const classes: string = cn('breadcrumb-item', {
		'breadcrumb-item__last': !!lastItem
	})

	return (
		<li className={ classes }>
			{
				lastItem
				? <span className={ styles['breadcrumb-title'] }>{ title }</span>
				: <Link to={ to } className={ styles['breadcrumb-title'] }>{ title }</Link>
			}

			{
				!lastItem && (
					<span className={ styles['breadcrumb-separator'] }>
						<FontAwesomeIcon icon="angle-right" />
					</span>
				)
			}
		</li>
	)
}

export default BreadcrumbItem
