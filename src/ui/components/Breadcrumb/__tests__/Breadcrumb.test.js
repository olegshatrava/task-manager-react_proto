import React from 'react'
import { shallow } from 'enzyme'

import { Breadcrumb } from '../Breadcrumb'

describe('UI Breadcrumb', () => {
	let initialProps = {}

	const routes = {
		'/one': 'Уровень 1',
		'/one/two': 'Уровень 2',
		'/one/two/three': 'Уровень 3'
	}

	beforeAll(() => {
		initialProps = {
			routes,
			location: {
				pathname: '/one/two'
			}
		}
	})

	it('should render without problem', () => {
		const wrapper = shallow(<Breadcrumb { ...initialProps } />)

		expect(wrapper).toMatchSnapshot()
	})

	it('should render 2 BreadcrumbItem', () => {
		const wrapper = shallow(<Breadcrumb { ...initialProps } />)

		expect(wrapper.find('BreadcrumbItem')).toHaveLength(2)
	})

	it('should render 3 BreadcrumbItem', () => {
		initialProps.location.pathname = '/one/two/three'

		const wrapper = shallow(<Breadcrumb { ...initialProps } />)

		const item = wrapper.find('BreadcrumbItem')

		expect(item).toHaveLength(3)
		expect(item.at(0).props().lastItem).toEqual(false)
		expect(item.at(1).props().lastItem).toEqual(false)
		expect(item.at(2).props().lastItem).toEqual(true)
	})
})
