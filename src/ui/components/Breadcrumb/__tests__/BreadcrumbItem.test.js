import React from 'react'
import { shallow, mount } from 'enzyme'

import BreadcrumbItem from '../BreadcrumbItem'

describe('UI Breadcrumb', () => {
	let initialProps = {}

	beforeAll(() => {
		initialProps = {
			to: '/one',
			title: 'Уровень 1',
			lastItem: true
		}
	})

	it('should render without problem', () => {
		const wrapper = shallow(<BreadcrumbItem { ...initialProps } />)

		expect(wrapper).toMatchSnapshot()
	})

	it('render separator for lastItem=true', () => {
		const wrapper = mount(<BreadcrumbItem { ...initialProps } />)

		expect(wrapper.props().lastItem).toEqual(true)
		expect(wrapper.find('.breadcrumb-item').hasClass('breadcrumb-item__last')).toEqual(true)
		expect(wrapper.find('.breadcrumb-title').text()).toEqual(initialProps.title)
		expect(wrapper.find('.breadcrumb-separator')).toHaveLength(0)
	})

	it('render separator for lastItem=false', () => {
		const wrapper = shallow(<BreadcrumbItem { ...initialProps } lastItem={ false } />)

		expect(wrapper.find('Link').props()).toEqual({ to: initialProps.to, children: initialProps.title, replace: false, className: 'breadcrumb-title' })
		expect(wrapper.find('.breadcrumb-item').hasClass('breadcrumb-item__last')).toEqual(false)
		expect(wrapper.find('.breadcrumb-separator')).toHaveLength(1)
	})
})
