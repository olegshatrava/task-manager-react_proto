import React from 'react'
import { shallow } from 'enzyme'

import Select from '../Select'

describe('UI Select', () => {
	it('should render without problem.', () => {
		const wrapper = shallow(
			<Select placeholder="Выберите опцию" onChange={ jest.fn() }>
				<Select.Option value="test1">Тестовая опция 1</Select.Option>
				<Select.Option value="test2">Тестовая опция 2</Select.Option>
			</Select>
		)

		expect(wrapper).toMatchSnapshot()
	})
})
