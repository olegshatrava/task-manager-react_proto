// @flow
import React, { type Node, Component } from 'react'
import RcSelect, { Option, OptGroup } from 'rc-select'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import classnames from 'classnames/bind'

import styles from './styles/Select.scss'
const cn = classnames.bind(styles)

export type SelectProps = {
	className?: string,
	id?: string,
	size: 'sm' | 'md' | 'lg',
	mode: 'default' | 'tags' | 'multiple',
	tabIndex?: number;
	optionLabelProp: any,
	showSearch: boolean,
	allowClear?: boolean,
	showArrow?: boolean,
	notFoundContent?: string,
	disabled?: boolean,
	defaultActiveFirstOption?: boolean
}

class Select extends Component<SelectProps> {
	static Option = Option
	static OptGroup = OptGroup

	static defaultProps = {
		size: 'md',
		showSearch: false,
		transitionName: 'slide-up',
		choiceTransitionName: 'zoom',
		notFoundContent: 'Ничего не найдено'
	}

	rcSelect: any

	setRefRcSelect = (node: any) => {
		this.rcSelect = node
	}

	focus = () => {
		this.rcSelect.focus()
	}

	blur = () => {
		this.rcSelect.blur()
	}

	render() {
		const { size, mode, optionLabelProp, notFoundContent, className, ...restProps } = this.props

		const modeConfig = {
			multiple: mode === 'multiple',
			tags: mode === 'tags'
		}

		const inputIcon: Node = <FontAwesomeIcon icon="chevron-down" className={ `UiSelect-arrow-icon` } />
		const removeIcon: Node = <FontAwesomeIcon icon="times" className={ `UiSelect-arrow-icon` } />
		const menuItemSelectedIcon: Node = <FontAwesomeIcon icon="check" className={ `UiSelect-arrow-icon` } />

		const classes: string = cn(
			`UiSelect-size-${ size }`,
			className
		)

		return (
			<RcSelect
				ref={ this.setRefRcSelect }
				inputIcon={ inputIcon }
				removeIcon={ removeIcon }
				menuItemSelectedIcon={ menuItemSelectedIcon }
				{ ...restProps }
				{ ...modeConfig }
				prefixCls="UiSelect"
				className={ classes }
				optionLabelProp={ optionLabelProp || 'children' }
				notFoundContent={ notFoundContent }
			/>
		)
	}
}

export default Select
