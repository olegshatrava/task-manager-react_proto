# Select

## Select property
| Prop | Description | Type | Default | Required
|---|---|---|---|---|---|
| defaultValue | Значение по умолчанию | *{string}*  | - | - |
| size | Резмеры `sm`, `md`, `lg` | *{string}*  | `md` | - |
| placeholder | Placeholder | *{string}* | - | - |
| mode | Режим работы селекта `default`, `tags`, `multiple` | *{string}* | `default` | - |
| className | Передать внешние классы | *{string}* | - | - |
| onChange | Получение значений выбранного элемента | *{Function}* | - | + |
| showSearch | Поиск по опциям | *{boolean}* | - | - |
| notFoundContent | Текст результата поиска без резулататов | *{string}* | - | - |

## Option property
| Prop | Description | Type | Default | Required
|---|---|---|---|---|---|
| Title | Название | *{string}*  | - | - |
| value | Значение | *{string}*  | - | - |
| children | Описание | *{ReactNode}*  | - | - |
| disabled | Заблокировать | *{boolean}*  | false | - |


## Use
```jsx
import { Select } from 'ui/components'
or
import Select from 'ui/components/Select'

const onChange = (value) => console.log(value)

<Select placeholder="Select values" onChange={ onChange }>
	<Select.Option value="test">Test</Select.Option>
</Select>
```