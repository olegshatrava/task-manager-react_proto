// @flow
import React, { type Node, Component } from 'react'
import RcDropdown from 'rc-dropdown'
import classnames from 'classnames/bind'

import styles from './styles/Dropdown.scss'
const cn = classnames.bind(styles)

export type DropdownTrigger = ('click' | 'hover')[]
export type DropdownPlacement = 'topLeft' | 'topCenter' | 'topRight' | 'bottomLeft' | 'bottomCenter' | 'bottomRight'
export type DropdownTransitionName = 'slide-up' | 'slide-down' | string
export type DropdownProps = {
	trigger?: DropdownTrigger,
	placement?: DropdownPlacement,
	overlay: Node,
	visible?: boolean,
	disabled?: boolean,
	className?: string,
	transitionName?: DropdownTransitionName,
	children: any,
	onVisibleChange: (visible?: boolean) => void,
	getPopupContainer?: (triggerNode: Element) => HTMLElement
}

class Dropdown extends Component<DropdownProps> {
	static defaultProps = {
		mouseEnterDelay: 0.15,
		mouseLeaveDelay: 0.1,
		placement: 'bottomLeft'
	}

	getTransitionName = (): DropdownTransitionName => {
		const { placement, transitionName } = this.props

		if (transitionName !== undefined) {
			return transitionName
		}

		if (!!placement && placement.includes('top')) {
			return 'slide-down'
		}

		return 'slide-up'
	}

	render() {
		const { children, overlay: overlayElements, trigger, disabled } = this.props

		const child = React.Children.only(children)
		const overlay = React.Children.only(overlayElements)

		const dropdownTrigger = React.cloneElement(child, {
			className: cn(child.props.className, `Dropdown-trigger`),
			disabled
		})

		const triggerActions: ?DropdownTrigger = disabled ? [] : trigger

		const { selectable = false, focusable = true } = overlay.props

		const fixedModeOverlay: Node = typeof overlay.type === 'string'
		? overlay
		: React.cloneElement(overlay, {
			mode: 'vertical',
			selectable,
			focusable
		})

		return (
			<RcDropdown
				prefixCls={ 'Dropdown' }
				transitionName={ this.getTransitionName() }
				trigger={ triggerActions }
				overlay={ fixedModeOverlay }
				{ ...this.props }
			>
				{ dropdownTrigger }
			</RcDropdown>
		)
	}
}

export default Dropdown
