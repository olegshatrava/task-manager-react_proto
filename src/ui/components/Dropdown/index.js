import Dropdown from './Dropdown'
import Menu from './Menu'
import MenuItem from './MenuItem'

Dropdown.Menu = Menu
Dropdown.MenuItem = MenuItem

export default Dropdown
