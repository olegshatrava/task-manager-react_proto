import React from 'react'
import classnames from 'classnames/bind'

import styles from './styles/Dropdown.scss'
const cn = classnames.bind(styles)

const DropdownMenu = ({
	prefixCls,
	mode,
	onClick,
	className,
	children,
	...props
}) => {
	const classes = cn(`${ prefixCls }`, `${ prefixCls }-${ mode }`, className)

	return (
		<ul className={ classes } onClick={ onClick } role="menu">
			{ children }
		</ul>
	)
}

export default DropdownMenu
