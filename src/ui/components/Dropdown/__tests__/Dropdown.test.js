import React from 'react'
import { shallow } from 'enzyme'

import Dropdown from '../index'

describe('Ui Dropdown', () => {
	const overlay = (
		<Dropdown.Menu>
			<Dropdown.MenuItem>Link 1</Dropdown.MenuItem>
			<Dropdown.MenuItem>Link 2</Dropdown.MenuItem>
		</Dropdown.Menu>
	)

	it('should render without problem', () => {
		const wrapper = shallow(
			<Dropdown overlay={ overlay }>
				<a href="javascript:void(0)">Hover link</a>
			</Dropdown>
		)

		expect(wrapper).toMatchSnapshot()
	})
})
