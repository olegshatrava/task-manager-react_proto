import React from 'react'
import classnames from 'classnames/bind'

import styles from './styles/Dropdown.scss'
const cn = classnames.bind(styles)

const DropdownMenuItem = ({
	className,
	children,
	...props
}) => {
	const classes = cn(`Dropdown-menu-item`, className)

	return (
		<li className={ classes } role="menuitem">
			{ children }
		</li>
	)
}

export default DropdownMenuItem
