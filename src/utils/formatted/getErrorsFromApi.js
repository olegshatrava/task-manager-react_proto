import camelCase from 'utils/string/camelCase'

export default function getErrorsFromApi(validationError) {
	let nextErrors = []

	for (let error in validationError) {
		if (validationError.hasOwnProperty(error)) {
			nextErrors = [...nextErrors, error]
		}
	}

	return nextErrors.reduce((errors, error) => {
		const valueError = validationError[error]

		return {
			...errors,
			[camelCase(error)]: Array.isArray(valueError) ? valueError[0] : valueError
		}
	}
	, {})
}
