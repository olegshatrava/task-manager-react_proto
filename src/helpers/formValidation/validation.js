import './setYupLocale'

/**
* Validation values with Yup schema.
* @param {Function} getValidationSchema - Get yup validation schema
* @return {Object} - Errors list
*/
export default function validate(getValidationSchema) {
	return (values) => {
		const validationSchema = getValidationSchema(values)

		try {
			validationSchema.validateSync(values, { abortEarly: false })
			return {}
		} catch (error) {
			return getErrorsFromValidationError(error)
		}
	}
}

/**
* Formatted errors for form.
* @param {Object} validationError - Validation yup error
* @return {Object} - Errors
*/
export function getErrorsFromValidationError(validationError) {
	return validationError.inner.reduce((errors, error) => ({
		...errors,
		[error.path]: error.errors[0]
	}), {})
}
