/* eslint-disable */
import { setLocale } from 'yup'

export default setLocale({
	mixed: {
		required: 'Поле обязательно для заполнения'
	},
	string: {
		email: 'Некорректный email адрес',
		min: 'Поле должно быть больше ${min} символов',
		max: 'Поле не должно превышать ${min} символов'
	}
})
