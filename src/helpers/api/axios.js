/* eslint no-param-reassign: 0 */
import axios from 'axios'
import jsCookie from 'js-cookie'

/*=============================================*/
/*                BASE SETTINS API             */
/*=============================================*/

const instance = axios.create({
  baseURL: 'path'
})

/*=============================================*/
/*     Interceptor REQ Authorization Header    */
/*=============================================*/
instance.interceptors.request.use(function(config) {
	const token = jsCookie.get('token')

	if (token) {
		config.headers.common['Authorization'] = `Bearer ${ token }`
	}

    return config
}, function(err) {
	return Promise.reject(err)
})

/*==============================================*/
/*     Interceptor RES Authorization Header     */
/*==============================================*/
instance.interceptors.response.use(function(response) {
	const newToken = response.headers.authorization

	if (newToken) {
		jsCookie.set('token', newToken.replace('Bearer ', ''), { expires: 13 })
	}

    return response
}, function(err) {
	const { status } = err.response

	if (status === 500) {
		err.response.data = {
			result: 'error',
			message: 'Произошла ошибка, попробуйте позже.'
		}
	}

	return Promise.reject(err)
})

export default instance
