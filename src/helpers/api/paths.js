export const paths = {
	// Authentication
	login: '/auth/login',
	recoveryPasswordEmail: '/auth/reset_password/email',
	recoveryPassword: '/auth/reset_password',

	// User info
	userInfo: '/profile',
	changeUserPhoto: '/profile/avatar',
	userSecurity: '/profile/security',

	// Tasks
	tasks: '/tasks',

	// Faild requests
	badRequest: '/bad_request'
}
