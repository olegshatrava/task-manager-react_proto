import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import createHistory from 'history/createBrowserHistory'

// Import root component
import App from './modules/App'

// Import root styles
import './styles/root'

// Import store configure
import { configureStore } from './store/configureStore'

const initialState = {}
const history = createHistory()
const store = configureStore(initialState, history)

const renderMethod = module.hot ? ReactDOM.render : ReactDOM.hydrate
const node = document.getElementById('application')

renderMethod(
	<Provider store={ store }>
		<ConnectedRouter history={ history }>
			<App />
		</ConnectedRouter>
	</Provider>,
	node
)
