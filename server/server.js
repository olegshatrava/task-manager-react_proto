import path from 'path'
import express from 'express'
import expressStaticGzip from 'express-static-gzip'
import webpack from 'webpack'
import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'

import webpackConfig from '../tools/webpack/webpack.base.babel'
import paths from '../tools/paths'
import getClientEnvironment from '../tools/env'
import { log } from '../tools/utils/log'

const env = getClientEnvironment()
const app = express()

app.disable('x-powered-by')

const NODE_ENV = env.raw['NODE_ENV']
const HOST = env.raw['HOST']
const PORT = env.raw['PORT']

if (NODE_ENV !== 'production') {
	const clientConfig = webpackConfig({ target: 'client' })
	const compiler = webpack(clientConfig)

	app.use(webpackDevMiddleware(compiler, {
		publicPath: clientConfig.output.publicPath,
		noInfo: true,
		hot: true,
		historyApiFallback: true,
		watchOptions: {
			aggregateTimeout: 300,
			poll: true
		}
	}))

	app.use(webpackHotMiddleware(compiler, {
		reload: true,
		log: false
	}))

	app.use('*', (req, res, next) => {
		const filename = path.join(compiler.outputPath, 'index.html')

		compiler.outputFileSystem.readFile(filename, (err, result) => {
			if (err) {
				return next(err)
			}
			res.set('content-type', 'text/html')
			res.send(result)
			res.end()
		})
	})
}

if (NODE_ENV === 'production') {
	// Compress gzip and brotli static
	app.use(expressStaticGzip(paths.buildUrl, {
		// enableBrotli: true
	}))

	app.get('*', (req, res) => {
		res.sendFile(`${ paths.buildUrl }/index.html`)
	})
}

app.listen(PORT, (err) => {
	if (err) {
		global.console.error(err)
		return
	}

	log({
		level: 'info',
		title: 'Server',
		message: `

		🌎

		Server is now listening on port: ${ PORT }.
		Running environment: ${ NODE_ENV }.
		You can access it in the browser at http://${ HOST }:${ PORT }

		Press Ctrl-C to stop server.

		`
	})
})
